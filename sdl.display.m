:- module sdl.display.


:- interface.
:- use_module io.

	% sdl_get_display_dpi(Dispay, DDPI, HDPI, VDPI):
	%
	% Get the display dots-per-inch values. The three DPI values are
	% the diagonal, horizontal and vertical DPI respectively.
	%
:- pred sdl_get_display_dpi(int, float, float, float, io.io, io.io).
:- mode sdl_get_display_dpi(in, out, out, out, di, uo) is det.


:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").

:- pragma promise_pure(sdl_get_display_dpi/6).
sdl_get_display_dpi(Display, DDPI, HDPI, VDPI, !IO) :-
	impure foreign_sdl_get_display_dpi(Display, DDPI, HDPI, VDPI).

:- pragma inline(foreign_sdl_get_display_dpi/4).
:- impure pred foreign_sdl_get_display_dpi(int::in, float::out, float::out, float::out) is det.
:- pragma foreign_proc("C",
	foreign_sdl_get_display_dpi(Display::in, DDPI::out, HDPI::out, VDPI::out),
	[will_not_call_mercury],
	"
	float ddpi, hdpi, vdpi;
	SDL_GetDisplayDPI(Display, &ddpi, &hdpi, &vdpi);
	DDPI = (MR_Float) ddpi;
	HDPI = (MR_Float) hdpi;
	VDPI = (MR_Float) vdpi;
	"
).

:- end_module sdl.display.
