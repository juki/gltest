%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.events.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
% This module handles SDL events. Notice that only the events needed
% for the game are supported.
%
%--------------------------------------------------%
:- module sdl.events.

%--------------------------------------------------%
:- interface.
:- import_module list, bool, univ, maybe.
:- use_module io.

:- use_module
	sdl.mouse,
	sdl.keyboard,
	sdl.window,
	sdl.audio.

:- type error ---> error(string).

	% A more readable name for SDL timestamps.
:- type timestamp == uint32.

:- type button_state
	--->	pressed
	;	released.

	% Event handler predicate types and insts.
:- type idle_handler == pred(bool, io.io, io.io).
:- inst idle_handler == (pred(out, di, uo) is det).
:- type window_0_data_handler == pred(bool, timestamp, sdl.window.window_id, io.io, io.io).
:- inst window_0_data_handler == (pred(out, in, in, di, uo) is det).
:- type window_2_data_handler == pred(bool, timestamp, sdl.window.window_id, int32, int32, io.io, io.io).
:- inst window_2_data_handler == (pred(out, in, in, in, in, di, uo) is det).
:- type window_resized_handler == window_2_data_handler.
:- inst window_resized_handler == window_2_data_handler.
:- type window_size_changed_handler == window_2_data_handler.
:- inst window_size_changed_handler == window_2_data_handler.
:- type window_enter_handler == window_0_data_handler.
:- inst window_enter_handler == window_0_data_handler.
:- type window_leave_handler == window_0_data_handler.
:- inst window_leave_handler == window_0_data_handler.
:- type window_focus_gained_handler == window_0_data_handler.
:- inst window_focus_gained_handler == window_0_data_handler.
:- type window_focus_lost_handler == window_0_data_handler.
:- inst window_focus_lost_handler == window_0_data_handler.
:- type window_close_handler == window_0_data_handler.
:- inst window_close_handler == window_0_data_handler.
:- type window_take_focus_handler == window_0_data_handler.
:- inst window_take_focus_handler == window_0_data_handler.
:- type quit_handler == pred(bool, timestamp, io.io, io.io).
:- inst quit_handler == (pred(out, in, di, uo) is det).
:- type mouse_motion_handler == pred(bool, timestamp, sdl.window.window_id, uint32, sdl.mouse.mouse_button_state, int32, int32, int32, int32, io.io, io.io).
:- inst mouse_motion_handler == (pred(out, in, in, in, in, in, in, in, in, di, uo) is det).
:- type mouse_button_handler == pred(bool, timestamp, sdl.window.window_id, uint32, sdl.mouse.mouse_button, button_state, uint8, int32, int32, io.io, io.io).
:- inst mouse_button_handler == (pred(out, in, in, in, in, in, in, in, in, di, uo) is det).
:- type mouse_button_down_handler == mouse_button_handler.
:- inst mouse_button_down_handler == mouse_button_handler.
:- type mouse_button_up_handler == mouse_button_handler.
:- inst mouse_button_up_handler == mouse_button_handler.
:- type mouse_wheel_handler == pred(bool, timestamp, sdl.window.window_id, uint32, int32, int32, sdl.mouse.mouse_wheel_direction, io.io, io.io).
:- inst mouse_wheel_handler == (pred(out, in, in, in, in, in, in, di, uo) is det).
:- type key_handler == pred(bool, timestamp, sdl.window.window_id, button_state, uint8, sdl.keyboard.scancode, sdl.keyboard.keymods, io.io, io.io).
:- inst key_handler == (pred(out, in, in, in, in, in, in, di, uo) is det).
:- type key_up_handler == key_handler.
:- inst key_up_handler == key_handler.
:- type key_down_handler == key_handler.
:- inst key_down_handler == key_handler.
:- type text_input_handler == pred(bool, timestamp, sdl.window.window_id, string, io.io, io.io).
:- inst text_input_handler == (pred(out, in, in, in, di, uo) is det).
:- type audio_device_handler == pred(bool, timestamp, uint32, sdl.audio.audio_device_type, io.io, io.io).
:- inst audio_device_handler == (pred(out, in, in, in, di, uo) is det).
:- type audio_device_added_handler == audio_device_handler.
:- inst audio_device_added_handler == audio_device_handler.
:- type audio_device_removed_handler == audio_device_handler.
:- inst audio_device_removed_handler == audio_device_handler.
:- type common_handler == pred(bool, timestamp, io.io, io.io).
:- inst common_handler == (pred(out, in, di, uo) is det).
:- type keymap_changed_handler == common_handler.
:- inst keymap_changed_handler == common_handler.
:- type user_event_handler == pred(bool, timestamp, int32, univ, univ, io.io, io.io).
:- inst user_event_handler == (pred(out, in, in, in, in, di, uo) is det).

:- type user_event_type.

	% user_event_type_to_uint32(UEType) = Num
	%
	% Get the uint32 identifier for this type. Needed in case one
	% wants to push events from foreign code.
	%
:- func user_event_type_to_uint32(user_event_type) = uint32.

	% register_events(N, EventTypes, !IO):
	%
	% Register `N` user event types. `EventTypes` will be a list of
	% `user_event_type`s, or an empty list if there weren't enough
	% event types left.
	%
:- pred register_events(int, list(user_event_type), io.io, io.io).
:- mode register_events(in, out, di, uo) is det.

	% push_user_event(Num, Code, Data1, Data2, Result, !IO):
	%
	% Push an event registered with `register_events`.
	%
:- pred push_user_event(user_event_type, int32, univ, univ, maybe_error, io.io, io.io).
:- mode push_user_event(in, in, in, in, out, di, uo) is det.

	% push_quit_event(!IO):
	%
	% Push a quit event.
	%
:- pred push_quit_event(io.io::di, io.io::uo) is det.

:- type event_handler
	--->	idle(pred(bool::out, io.io::di, io.io::uo) is det)
	;	window_resized(pred(bool::out, timestamp::in, sdl.window.window_id::in, int32::in, int32::in, io.io::di, io.io::uo) is det)
	;	window_size_changed(pred(bool::out, timestamp::in, sdl.window.window_id::in, int32::in, int32::in, io.io::di, io.io::uo) is det)
	;	window_enter(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo) is det)
	;	window_leave(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo) is det)
	;	window_focus_gained(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo) is det)
	;	window_focus_lost(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo) is det)
	;	window_close(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo) is det)
	;	window_take_focus(pred(bool::out, timestamp::in, sdl.window.window_id::in, io.io::di, io.io::uo ) is det)
	;	key_up(pred(bool::out, timestamp::in, sdl.window.window_id::in, button_state::in, uint8::in, sdl.keyboard.scancode::in, sdl.keyboard.keymods::in, io.io::di, io.io::uo) is det)
	;	key_down(pred(bool::out, timestamp::in, sdl.window.window_id::in, button_state::in, uint8::in, sdl.keyboard.scancode::in, sdl.keyboard.keymods::in, io.io::di, io.io::uo) is det)
	;	text_input(pred(bool::out, timestamp::in, sdl.window.window_id::in, string::in, io.io::di, io.io::uo) is det)
	;	keymap_changed(pred(bool::out, timestamp::in, io.io::di, io.io::uo) is det)
	;	mouse_motion(pred(bool::out, timestamp::in, sdl.window.window_id::in, uint32::in, uint32::in, int32::in, int32::in, int32::in, int32::in, io.io::di, io.io::uo) is det)
	;	mouse_button_down(pred(bool::out, timestamp::in, sdl.window.window_id::in, uint32::in, sdl.mouse.mouse_button::in, button_state::in, uint8::in, int32::in, int32::in, io.io::di, io.io::uo) is det)
	;	mouse_button_up(pred(bool::out, timestamp::in, sdl.window.window_id::in, uint32::in, sdl.mouse.mouse_button::in, button_state::in, uint8::in, int32::in, int32::in, io.io::di, io.io::uo) is det)
	;	mouse_wheel(pred(bool::out, timestamp::in, sdl.window.window_id::in, uint32::in, int32::in, int32::in, sdl.mouse.mouse_wheel_direction::in, io.io::di, io.io::uo) is det)
	;	audio_device_added(pred(bool::out, timestamp::in, uint32::in, sdl.audio.audio_device_type::in, io.io::di, io.io::uo) is det)
	;	audio_device_removed(pred(bool::out, timestamp::in, uint32::in, sdl.audio.audio_device_type::in, io.io::di, io.io::uo) is det)
	;	quit(pred(bool::out, timestamp::in, io.io::di, io.io::uo) is det)
	;	user_event(user_event_type, pred(bool::out, timestamp::in, int32::in, univ::in, univ::in, io.io::di, io.io::uo) is det).

	% fold_events(Handlers, !Acc, !IO):
	%
	% Handle events.
	%
:- pred fold_events(list(event_handler)::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.
:- import_module int, uint, uint32, hash_table, require.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").

:- pragma foreign_import_module("C", sdl.audio).

:- type sdl_event.
:- pragma foreign_type("C", sdl_event, "SDL_Event*").

	% This is needed for both mouse and keyboard events, so it doesn't
	% fit in either of those modules.
:- pragma foreign_enum("C", button_state/0, [
	pressed  - "SDL_PRESSED",
	released - "SDL_RELEASED"
]).


%--------------------------------------------------%
% Event types.

:- type event_type
	--->	quit
	;	window_resized
	;	window_size_changed
	;	window_enter
	;	window_leave
	;	window_focus_gained
	;	window_focus_lost
	;	window_close
	;	window_take_focus
	;	key_down
	;	key_up
	;	text_input
	;	keymap_changed
	;	mouse_motion
	;	mouse_button_down
	;	mouse_button_up
	;	mouse_wheel
	;	audio_device_added
	;	audio_device_removed
	;	user_event.

:- pragma foreign_export_enum("C", event_type/0, [prefix("GLTEST_SDL_"), uppercase], []).

	% event_type(Event, Type):
	%
	% Retrieve the `event_type` of `Event`.
	%
:- pred event_type(sdl_event::in, event_type::out) is semidet.
:- pragma foreign_proc("C", event_type(Event::in, Type::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Uint8 event;
	switch (Event->type) {
	case SDL_QUIT:
		Type = GLTEST_SDL_QUIT;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_WINDOWEVENT:
		event = Event->window.event;
		if (event == SDL_WINDOWEVENT_RESIZED) Type = GLTEST_SDL_WINDOW_RESIZED;
		else if (event == SDL_WINDOWEVENT_SIZE_CHANGED) Type = GLTEST_SDL_WINDOW_SIZE_CHANGED;
		else if (event == SDL_WINDOWEVENT_ENTER) Type = GLTEST_SDL_WINDOW_ENTER;
		else if (event == SDL_WINDOWEVENT_LEAVE) Type = GLTEST_SDL_WINDOW_LEAVE;
		else if (event == SDL_WINDOWEVENT_FOCUS_GAINED) Type = GLTEST_SDL_WINDOW_FOCUS_GAINED;
		else if (event == SDL_WINDOWEVENT_FOCUS_LOST) Type = GLTEST_SDL_WINDOW_FOCUS_LOST;
		else if (event == SDL_WINDOWEVENT_CLOSE) Type = GLTEST_SDL_WINDOW_CLOSE;
		else if (event == SDL_WINDOWEVENT_TAKE_FOCUS) Type = GLTEST_SDL_WINDOW_TAKE_FOCUS;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_KEYDOWN:
		Type = GLTEST_SDL_KEY_DOWN;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_KEYUP:
		Type = GLTEST_SDL_KEY_UP;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_TEXTINPUT:
		Type = GLTEST_SDL_TEXT_INPUT;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_KEYMAPCHANGED:
		Type = GLTEST_SDL_KEYMAP_CHANGED;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_MOUSEMOTION:
		Type = GLTEST_SDL_MOUSE_MOTION;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_MOUSEBUTTONDOWN:
		Type = GLTEST_SDL_MOUSE_BUTTON_DOWN;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_MOUSEBUTTONUP:
		Type = GLTEST_SDL_MOUSE_BUTTON_UP;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_MOUSEWHEEL:
		Type = GLTEST_SDL_MOUSE_WHEEL;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_AUDIODEVICEADDED:
		Type = GLTEST_SDL_AUDIO_DEVICE_ADDED;
		SUCCESS_INDICATOR = 1;
		break;
	case SDL_AUDIODEVICEREMOVED:
		Type = GLTEST_SDL_AUDIO_DEVICE_REMOVED;
		SUCCESS_INDICATOR = 1;
		break;
	default:
		if (Event->type >= SDL_USEREVENT) {
			Type = GLTEST_SDL_USER_EVENT;
			SUCCESS_INDICATOR = 1;
		} else {
			SUCCESS_INDICATOR = 0;
		}
	}
	"
).

%--------------------------------------------------%
% Quit events

:- pragma foreign_proc("C", push_quit_event(IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	SDL_Event event = {
		.quit.type = SDL_QUIT
	};
	SDL_PushEvent(&event);
	IO1 = IO0;
	"
).

%--------------------------------------------------%
% User events

:- type user_event_type ---> user_event_type(uint32).

user_event_type_to_uint32(user_event_type(Num)) = Num.

:- pred sdl_user_event_number(sdl_event::in, uint32::out) is semidet.
:- pragma foreign_proc("C", sdl_user_event_number(Event::in, Number::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	uint32_t type = Event->type;
	if (type >= SDL_USEREVENT) {
		Number = type;
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- func user_event_types_list(uint32, int, list(user_event_type)) = list(user_event_type).
user_event_types_list(Start, N, Acc) =
	(	N = 0
	->	Acc
	;	user_event_types_list(
			Start + 1u32,
			N - 1,
			[user_event_type(Start) | Acc]
		)
	).

:- pragma promise_pure(register_events/4).
register_events(N, Nums, !IO) :-
	(	impure sdl_register_events(N, First)
	->	Nums = user_event_types_list(First, N, [])
	;	Nums = []
	).

:- pragma inline(sdl_register_events/2).
:- impure pred sdl_register_events(int::in, uint32::out) is semidet.
:- pragma foreign_proc("C", sdl_register_events(N::in, FirstNum::out),
	[will_not_call_mercury],
	"
	uint32_t first_num = SDL_RegisterEvents(N);
	if (first_num == (Uint32)-1) {
		SUCCESS_INDICATOR = 0;
	} else {
		FirstNum = first_num;
		SUCCESS_INDICATOR = 1;
	}
	"
).

:- pragma promise_pure(push_user_event/7).
push_user_event(user_event_type(TypeNum), Code, Data1, Data2, Result, !IO) :-
	(	impure sdl_push_user_event(TypeNum, Code, Data1, Data2)
	->	Result = ok
	;	Result = error(sdl.get_error(!.IO, !:IO))
	).

:- impure pred sdl_push_user_event(uint32::in, int32::in, univ::in, univ::in) is semidet.
:- pragma foreign_proc("C", sdl_push_user_event(TypeNum::in, Code::in, Data1::in, Data2::in),
	[will_not_call_mercury, thread_safe],
	"
	SDL_Event event = {
		.user.type  = TypeNum,
		.user.code  = Code,
		.user.data1 = (void*) Data1,
		.user.data2 = (void*) Data2
	};
	if (SDL_PushEvent(&event) == -1) {
		SUCCESS_INDICATOR = 0;
	} else {
		SUCCESS_INDICATOR = 1;
	}
	"
).

%--------------------------------------------------%
% Event unpacking

:- pred sdl_common_event(sdl_event::in, timestamp::out) is det.
:- pragma foreign_proc("C", sdl_common_event(CommonEvent::in, Timestamp::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = CommonEvent->common.timestamp;
	"
).

:- pred sdl_window_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, int32::out, int32::out) is det.
:- pragma foreign_proc("C",
	sdl_window_event(WindowEvent::in, Timestamp::out, WindowID::out, Data1::out, Data2::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = WindowEvent->window.timestamp;
	WindowID = WindowEvent->window.windowID;
	Data1 = WindowEvent->window.data1;
	Data2 = WindowEvent->window.data2;
	"
).

:- pred sdl_keyboard_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, button_state::out, uint8::out, sdl.keyboard.scancode::out, sdl.keyboard.keymods::out) is det.
:- pragma foreign_proc("C",
	sdl_keyboard_event(KeyboardEvent::in, Timestamp::out, WindowID::out, State::out, Repeat::out, Scancode::out, KMods::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = KeyboardEvent->key.timestamp;
	WindowID = KeyboardEvent->key.windowID;
	State = KeyboardEvent->key.state;
	Repeat = KeyboardEvent->key.repeat;
	SDL_Keysym keysym = KeyboardEvent->key.keysym;
	Scancode = keysym.scancode;
	KMods = keysym.mod;
	"
).

:- pred sdl_text_input_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, string::out) is det.
:- pragma foreign_proc("C",
	sdl_text_input_event(TextInputEvent::in, Timestamp::out, WindowID::out, Text::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = TextInputEvent->text.timestamp;
	WindowID = TextInputEvent->text.windowID;
	char* text = TextInputEvent->text.text;
	Text = calloc(strnlen(text, SDL_TEXTINPUTEVENT_TEXT_SIZE), sizeof(char));
	strcpy_s(Text, SDL_TEXTINPUTEVENT_TEXT_SIZE, text);
	"
).

:- pred sdl_mouse_motion_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, uint32::out, sdl.mouse.mouse_button_state::out, int32::out, int32::out, int32::out, int32::out) is det.
:- pragma foreign_proc("C",
	sdl_mouse_motion_event(MouseMotionEvent::in, Timestamp::out, WindowID::out, Which::out, State::out, X::out, Y::out, Xrel::out, Yrel::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = MouseMotionEvent->motion.timestamp;
	WindowID = MouseMotionEvent->motion.windowID;
	Which = MouseMotionEvent->motion.which;
	State = MouseMotionEvent->motion.state;
	X = MouseMotionEvent->motion.x;
	Y = MouseMotionEvent->motion.y;
	Xrel = MouseMotionEvent->motion.xrel;
	Yrel = MouseMotionEvent->motion.yrel;
	"
).

:- pred sdl_mouse_button_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, uint32::out, sdl.mouse.mouse_button::out, button_state::out, uint8::out, int32::out, int32::out) is det.
:- pragma foreign_proc("C",
	sdl_mouse_button_event(MouseButtonEvent::in, Timestamp::out, WindowID::out, Which::out, Button::out, State::out, Clicks::out, X::out, Y::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = MouseButtonEvent->button.timestamp;
	WindowID = MouseButtonEvent->button.windowID;
	Which = MouseButtonEvent->button.which;
	Button = MouseButtonEvent->button.button;
	State = MouseButtonEvent->button.state;
	Clicks = MouseButtonEvent->button.clicks;
	X = MouseButtonEvent->button.x;
	Y = MouseButtonEvent->button.y;
	"
).

:- pred sdl_mouse_wheel_event(sdl_event::in, timestamp::out, sdl.window.window_id::out, uint32::out, int32::out, int32::out, sdl.mouse.mouse_wheel_direction::out) is det.
:- pragma foreign_proc("C",
	sdl_mouse_wheel_event(MouseWheelEvent::in, Timestamp::out, WindowID::out, Which::out, X::out, Y::out, Direction::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = MouseWheelEvent->wheel.timestamp;
	WindowID = MouseWheelEvent->wheel.windowID;
	Which = MouseWheelEvent->wheel.which;
	X = MouseWheelEvent->wheel.x;
	Y = MouseWheelEvent->wheel.y;
	Direction = MouseWheelEvent->wheel.direction;
	"
).

:- pred sdl_audio_device_event(sdl_event::in, timestamp::out, uint32::out, sdl.audio.audio_device_type::out) is det.
:- pragma foreign_proc("C",
	sdl_audio_device_event(AudioDeviceEvent::in, Timestamp::out, Which::out, IsCapture::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = AudioDeviceEvent->adevice.timestamp;
	Which = AudioDeviceEvent->adevice.which;
	IsCapture = (AudioDeviceEvent->adevice.iscapture == 0) ? GLTEST_SDL_OUTPUT : GLTEST_SDL_CAPTURE;
	"
).

:- pred sdl_quit_event(sdl_event::in, timestamp::out) is det.
:- pragma foreign_proc("C", sdl_quit_event(QuitEvent::in, Timestamp::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"Timestamp = QuitEvent->quit.timestamp;"
).

:- pred sdl_user_event(sdl_event::in, timestamp::out, int32::out, univ::out, univ::out) is det.
:- pragma foreign_proc("C",
	sdl_user_event(UserEvent::in, Timestamp::out, Code::out, Data1::out, Data2::out),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Timestamp = UserEvent->user.timestamp;
	Code = UserEvent->user.code;
	Data1 = (MR_Word) UserEvent->user.data1;
	Data2 = (MR_Word) UserEvent->user.data2;
	"
).

%--------------------------------------------------%
% Event loop

% Event handlers are given by the user as a list of
% `event_handler(T)`s. The list is converted into a hash table for
% more efficient access inside the event loop. We have to be able to
% write the keys down in the code, but we also want efficient lookups,
% so we use the type `handler_key` to write them and the function
% `handler_key_to_uint` to convert them to the corresponding unsigned
% integer.

:- type handler_key
	--->	idle
	;	builtin(event_type)
	;	user_event(uint32).

:- pragma inline(handler_key_to_uint/1).
:- func handler_key_to_uint(handler_key) = uint.
handler_key_to_uint(idle) = 0u.
handler_key_to_uint(builtin(window_resized)) = 5u.
handler_key_to_uint(builtin(window_size_changed)) = 6u.
handler_key_to_uint(builtin(window_enter)) = 10u.
handler_key_to_uint(builtin(window_leave)) = 11u.
handler_key_to_uint(builtin(window_focus_gained)) = 12u.
handler_key_to_uint(builtin(window_focus_lost)) = 13u.
handler_key_to_uint(builtin(window_close)) = 14u.
handler_key_to_uint(builtin(window_take_focus)) = 15u.
handler_key_to_uint(builtin(quit)) = 17u.
handler_key_to_uint(builtin(mouse_motion)) = 18u.
handler_key_to_uint(builtin(mouse_button_down)) = 19u.
handler_key_to_uint(builtin(mouse_button_up)) = 20u.
handler_key_to_uint(builtin(mouse_wheel)) = 21u.
handler_key_to_uint(builtin(key_down)) = 22u.
handler_key_to_uint(builtin(key_up)) = 23u.
handler_key_to_uint(builtin(text_input)) = 25u.
handler_key_to_uint(builtin(keymap_changed)) = 26u.
handler_key_to_uint(builtin(audio_device_added)) = 27u.
handler_key_to_uint(builtin(audio_device_removed)) = 28u.
handler_key_to_uint(builtin(user_event)) = 40u. % Not used, but needed for the compiler.
handler_key_to_uint(user_event(Num)) = 40u + cast_to_uint(Num).

:- pragma inline(handler_key/1).
:- func handler_key(event_handler) = uint.
handler_key(idle(_)) = handler_key_to_uint(idle).
handler_key(window_resized(_)) = handler_key_to_uint(builtin(window_resized)).
handler_key(window_size_changed(_)) = handler_key_to_uint(builtin(window_size_changed)).
handler_key(window_enter(_)) = handler_key_to_uint(builtin(window_enter)).
handler_key(window_leave(_)) = handler_key_to_uint(builtin(window_leave)).
handler_key(window_focus_gained(_)) = handler_key_to_uint(builtin(window_focus_gained)).
handler_key(window_focus_lost(_)) = handler_key_to_uint(builtin(window_focus_lost)).
handler_key(window_close(_)) = handler_key_to_uint(builtin(window_close)).
handler_key(window_take_focus(_)) = handler_key_to_uint(builtin(window_take_focus)).
handler_key(quit(_)) = handler_key_to_uint(builtin(quit)).
handler_key(mouse_motion(_)) = handler_key_to_uint(builtin(mouse_motion)).
handler_key(mouse_button_down(_)) = handler_key_to_uint(builtin(mouse_button_down)).
handler_key(mouse_button_up(_)) = handler_key_to_uint(builtin(mouse_button_up)).
handler_key(mouse_wheel(_)) = handler_key_to_uint(builtin(mouse_wheel)).
handler_key(key_down(_)) = handler_key_to_uint(builtin(key_down)).
handler_key(key_up(_)) = handler_key_to_uint(builtin(key_up)).
handler_key(text_input(_)) = handler_key_to_uint(builtin(text_input)).
handler_key(keymap_changed(_)) = handler_key_to_uint(builtin(keymap_changed)).
handler_key(audio_device_added(_)) = handler_key_to_uint(builtin(audio_device_added)).
handler_key(audio_device_removed(_)) = handler_key_to_uint(builtin(audio_device_removed)).
handler_key(user_event(user_event_type(Num), _)) = handler_key_to_uint(user_event(Num)).

:- func handlers_to_hash_table(
	list(event_handler)::in,
	hash_table(uint, event_handler)::hash_table_di
) = (hash_table(uint, event_handler)::hash_table_uo) is det.
handlers_to_hash_table([], Table) = Table.
handlers_to_hash_table([F | R], Table) =
	handlers_to_hash_table(R, set(Table, handler_key(F), F)).

:- pred handle_event_type(
	event_type::in,
	bool::out,
	hash_table(uint, event_handler)::hash_table_ui,
	sdl_event::in,
	io.io::di, io.io::uo
).
handle_event_type(quit, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(quit))) = quit(Handler)
	->	sdl_quit_event(Event, Timestamp),
		call(Handler, Continue, Timestamp, !IO)
	;	Continue = yes
	).
handle_event_type(window_resized, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_resized))) = window_resized(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, W, H),
		call(Handler, Continue, Timestamp, WindowID, W, H, !IO)
	;	Continue = yes
	).
handle_event_type(window_size_changed, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_size_changed))) = window_size_changed(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, W, H),
		call(Handler, Continue, Timestamp, WindowID, W, H, !IO)
	;	Continue = yes
	).
handle_event_type(window_enter, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_enter))) = window_enter(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(window_leave, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_leave))) = window_leave(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(window_focus_gained, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_focus_gained))) = window_focus_gained(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(window_focus_lost, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_focus_lost))) = window_focus_lost(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(window_close, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_close))) = window_close(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(window_take_focus, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(window_take_focus))) = window_take_focus(Handler)
	->	sdl_window_event(Event, Timestamp, WindowID, _, _),
		call(Handler, Continue, Timestamp, WindowID, !IO)
	;	Continue = yes
	).
handle_event_type(key_down, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(key_down))) = key_down(Handler)
	->	sdl_keyboard_event(Event, Timestamp, WindowID, BState, Repeat, Scancode, KMods),
		call(Handler, Continue, Timestamp, WindowID, BState, Repeat, Scancode, KMods, !IO)
	;	Continue = yes
	).
handle_event_type(key_up, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(key_up))) = key_up(Handler)
	->	sdl_keyboard_event(Event, Timestamp, WindowID, BState, Repeat, Scancode, KMods),
		call(Handler, Continue, Timestamp, WindowID, BState, Repeat, Scancode, KMods, !IO)
	;	Continue = yes
	).
handle_event_type(text_input, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(text_input))) = text_input(Handler)
	->	sdl_text_input_event(Event, Timestamp, WindowID, Text),
		call(Handler, Continue, Timestamp, WindowID, Text, !IO)
	;	Continue = yes
	).
handle_event_type(keymap_changed, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(keymap_changed))) = keymap_changed(Handler)
	->	sdl_common_event(Event, Timestamp),
		call(Handler, Continue, Timestamp, !IO)
	;	Continue = yes
	).
handle_event_type(mouse_motion, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(mouse_motion))) = mouse_motion(Handler)
	->	sdl_mouse_motion_event(Event, Timestamp, WindowID, Which, BState, X, Y, Xrel, Yrel),
		call(Handler, Continue, Timestamp, WindowID, Which, BState, X, Y, Xrel, Yrel, !IO)
	;	Continue = yes
	).
handle_event_type(mouse_button_down, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(mouse_button_down))) = mouse_button_down(Handler)
	->	sdl_mouse_button_event(Event, Timestamp, WindowID, Which, Button, BState, Clicks, X, Y),
		call(Handler, Continue, Timestamp, WindowID, Which, Button, BState, Clicks, X, Y, !IO)
	;	Continue = yes
	).
handle_event_type(mouse_button_up, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(mouse_button_up))) = mouse_button_up(Handler)
	->	sdl_mouse_button_event(Event, Timestamp, WindowID, Which, Button, BState, Clicks, X, Y),
		call(Handler, Continue, Timestamp, WindowID, Which, Button, BState, Clicks, X, Y, !IO)
	;	Continue = yes
	).
handle_event_type(mouse_wheel, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(mouse_wheel))) = mouse_wheel(Handler)
	->	sdl_mouse_wheel_event(Event, Timestamp, WindowID, Which, X, Y, Direction),
		call(Handler, Continue, Timestamp, WindowID, Which, X, Y, Direction, !IO)
	;	Continue = yes
	).
handle_event_type(audio_device_added, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(audio_device_added))) = audio_device_added(Handler)
	->	sdl_audio_device_event(Event, Timestamp, Which, DeviceType),
		call(Handler, Continue, Timestamp, Which, DeviceType, !IO)
	;	Continue = yes
	).
handle_event_type(audio_device_removed, Continue, Handlers, Event, !IO) :-
	(	search(Handlers, handler_key_to_uint(builtin(audio_device_removed))) = audio_device_removed(Handler)
	->	sdl_audio_device_event(Event, Timestamp, Which, DeviceType),
		call(Handler, Continue, Timestamp, Which, DeviceType, !IO)
	;	Continue = yes
	).
handle_event_type(user_event, Continue, Handlers, Event, !IO) :-
	(	sdl_user_event_number(Event, Num),
		search(Handlers, handler_key_to_uint(user_event(Num))) = user_event(user_event_type(Num), Handler)
	->	sdl_user_event(Event, Timestamp, Code, Data1, Data2),
		call(Handler, Continue, Timestamp, Code, Data1, Data2, !IO)
	;	Continue = yes
	).

:- pred handle_event(
	bool::out,
	hash_table(uint, event_handler)::hash_table_ui,
	sdl_event::in,
	io.io::di, io.io::uo
).
handle_event(Continue, Handlers, Event, !IO) :-
	(	event_type(Event, Type)
	->	handle_event_type(Type, Continue, Handlers, Event, !IO)
	;	% An unsupported event type.
		Continue = yes
	).

	% poll_event(Event, HasEvent, !IO):
	%
	% Retrieve the next event from the event queue, if there is
	% one. `HasEvent` is `yes` if a new event was stored in
	% `Event`. Doesn't wait if there are no events.
	%
:- pred poll_event(sdl_event::in, bool::out, io.io::di, io.io::uo) is det.
:- pragma inline(poll_event/4).
:- pragma foreign_proc("C", poll_event(Event::in, HasEvent::out, IO0::di, IO1::uo),
	% Mark as thread_safe, because the user is required to ensure this
	% is only used in the main thread anyway.
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	HasEvent = !SDL_PollEvent(Event) ? MR_NO : MR_YES;
	IO1 = IO0;
	"
).

:- pragma inline(alloc_event/3).
:- pred alloc_event(sdl_event::out, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", alloc_event(Event::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Event = malloc(sizeof(SDL_Event));
	IO1 = IO0;
	"
).

:- pragma inline(free_event/3).
:- pred free_event(sdl_event::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", free_event(Event::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	free(Event);
	IO1 = IO0;
	"
).

:- pred disable_unsupported_events(io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", disable_unsupported_events(IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"
	SDL_EventState(SDL_DROPFILE, SDL_IGNORE);
	SDL_EventState(SDL_DROPTEXT, SDL_IGNORE);
	SDL_EventState(SDL_DROPBEGIN, SDL_IGNORE);
	SDL_EventState(SDL_DROPCOMPLETE, SDL_IGNORE);
	SDL_EventState(SDL_SENSORUPDATE, SDL_IGNORE);
	SDL_EventState(SDL_SYSWMEVENT, SDL_IGNORE);
	SDL_EventState(SDL_DOLLARGESTURE, SDL_IGNORE);
	SDL_EventState(SDL_DOLLARRECORD, SDL_IGNORE);
	SDL_EventState(SDL_MULTIGESTURE, SDL_IGNORE);
	SDL_EventState(SDL_FINGERDOWN, SDL_IGNORE);
	SDL_EventState(SDL_FINGERUP, SDL_IGNORE);
	SDL_EventState(SDL_FINGERMOTION, SDL_IGNORE);
	SDL_EventState(SDL_JOYAXISMOTION, SDL_IGNORE);
	SDL_EventState(SDL_JOYBALLMOTION, SDL_IGNORE);
	SDL_EventState(SDL_JOYHATMOTION, SDL_IGNORE);
	SDL_EventState(SDL_JOYBUTTONDOWN, SDL_IGNORE);
	SDL_EventState(SDL_JOYBUTTONUP, SDL_IGNORE);
	SDL_EventState(SDL_JOYDEVICEADDED, SDL_IGNORE);
	SDL_EventState(SDL_JOYDEVICEREMOVED, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERAXISMOTION, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERBUTTONDOWN, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERBUTTONUP, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERDEVICEADDED, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERDEVICEREMOVED, SDL_IGNORE);
	SDL_EventState(SDL_CONTROLLERDEVICEREMAPPED, SDL_IGNORE);
	SDL_EventState(SDL_TEXTEDITING, SDL_IGNORE);
	SDL_EventState(SDL_CLIPBOARDUPDATE, SDL_IGNORE);
	SDL_EventState(SDL_RENDER_TARGETS_RESET, SDL_IGNORE);
	SDL_EventState(SDL_RENDER_DEVICE_RESET, SDL_IGNORE);
	IO1 = IO0;
	"
).

fold_events(Handlers, !IO) :-
	HandlerTable = handlers_to_hash_table(Handlers, init(uint_hash, 7, 0.8)),
	disable_unsupported_events(!IO),
	alloc_event(Event, !IO),
	(	search(HandlerTable, handler_key_to_uint(idle)) = idle(IdlePred)
	->	fold_events_with_idle(IdlePred, HandlerTable, Event, !IO)
	;	require.error("There is no idle pred for the event loop.")
	),
	free_event(Event, !IO).

:- pred fold_events_with_idle(
	pred(bool, io.io, io.io)::pred(out, di, uo) is det,
	hash_table(uint, event_handler)::hash_table_ui,
	sdl_event::in,
	io.io::di, io.io::uo
) is det.
fold_events_with_idle(IdlePred, Handlers, Event, !IO) :-
	poll_event(Event, HasEvent, !IO),
	(	HasEvent = yes,
		handle_event(Continue, Handlers, Event, !IO)
	;	HasEvent = no,
		call(IdlePred, Continue, !IO)
	),
	(	Continue = yes,
		fold_events_with_idle(IdlePred, Handlers, Event, !IO)
	;	Continue = no
	).

:- end_module sdl.events.
