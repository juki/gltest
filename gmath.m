%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: gmath.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module gmath.

%--------------------------------------------------%
:- interface.

%--------------------------------------------------%
% Types

:- type vec3(T)
	--->	vec3(vec3_x::T, vec3_y::T, vec3_z::T).

:- type vec4(T)
	--->	vec4(vec4_x::T, vec4_y::T, vec4_z::T, vec4_w::T).

:- type mat4(T) == vec4(vec4(T)).
:- type quat(T) == vec4(T).
:- type dual_quat(T)
	--->	dual_quat(quat(T), quat(T)).

:- type index_out_of_bounds
	--->	index_out_of_bounds(string).

%--------------------------------------------------%
% Constructors

:- func mat4f_identity = mat4(float).

:- func mat4f_scale(float, float, float) = mat4(float).
:- func mat4f_scale(mat4(float), float, float, float) = mat4(float).

:- func mat4f_translate(float, float, float) = mat4(float).
:- func mat4f_translate(mat4(float), float, float, float) = mat4(float).

:- func mat4f_rotate_x(float) = mat4(float).
:- func mat4f_rotate_x(mat4(float), float) = mat4(float).
:- func mat4f_rotate_y(float) = mat4(float).
:- func mat4f_rotate_y(mat4(float), float) = mat4(float).
:- func mat4f_rotate_z(float) = mat4(float).
:- func mat4f_rotate_z(mat4(float), float) = mat4(float).

:- func mat4f_frustum(float, float, float, float, float, float) = mat4(float).
:- func mat4f_ortho(float, float, float, float, float, float) = mat4(float).
:- func mat4f_perspective(float, float, float, float) = mat4(float).
:- func mat4f_look_at(vec3(float), vec3(float), vec3(float)) = mat4(float).

:- func quatf_identity = quat(float).

%--------------------------------------------------%
% Math

:- func vec3f_negate(vec3(float)) = vec3(float).
:- func vec4f_negate(vec4(float)) = vec4(float).

:- func vec3f_add(vec3(float), vec3(float)) = vec3(float).
:- func vec4f_add(vec4(float), vec4(float)) = vec4(float).

:- func vec3f_add_scalar(vec3(float), float) = vec3(float).
:- func vec4f_add_scalar(vec4(float), float) = vec4(float).

:- func vec3f_sub(vec3(float), vec3(float)) = vec3(float).
:- func vec4f_sub(vec4(float), vec4(float)) = vec4(float).

:- func vec3f_sub_scalar(vec3(float), float) = vec3(float).
:- func vec4f_sub_scalar(vec4(float), float) = vec4(float).

:- func vec3f_dot(vec3(float), vec3(float)) = float.
:- func vec4f_dot(vec4(float), vec4(float)) = float.

:- func vec3f_cross(vec3(float), vec3(float)) = vec3(float).

:- func vec3f_mul_scalar(vec3(float), float) = vec3(float).
:- func vec4f_mul_scalar(vec4(float), float) = vec4(float).

:- func vec3f_div_scalar(vec3(float), float) = vec3(float).
:- func vec4f_div_scalar(vec4(float), float) = vec4(float).

:- func vec3f_len(vec3(float)) = float.
:- func vec4f_len(vec4(float)) = float.

:- func vec3f_normalize(vec3(float)) = vec3(float).
:- func vec4f_normalize(vec4(float)) = vec4(float).

:- func mat4f_add(mat4(float), mat4(float)) = mat4(float).

:- func mat4f_add_scalar(mat4(float), float) = mat4(float).

:- func mat4f_sub(mat4(float), mat4(float)) = mat4(float).

:- func mat4f_sub_scalar(mat4(float), float) = mat4(float).

:- func mat4f_scale(mat4(float), float) = mat4(float).

:- func mat4f_mul(mat4(float), mat4(float)) = mat4(float).

:- func mat4f_mul_vec4f(mat4(float), vec4(float)) = vec4(float).

:- func quatf_add(quat(float), quat(float)) = quat(float).
:- func quatf_sub(quat(float), quat(float)) = quat(float).
:- func quatf_scale(quat(float), float) = quat(float).

%--------------------------------------------------%
% Utils

:- func vec3(T) ^ vec3_elem(int) = T.
:- func vec3(T) ^ vec3_elem(int) := T = vec3(T).

:- func vec4(T) ^ vec4_elem(int) = T.
:- func vec4(T) ^ vec4_elem(int) := T = vec4(T).

:- func mat4(T) ^ mat4_elem(int, int) = T.
:- func mat4(T) ^ mat4_elem(int, int) := T = mat4(T).

:- func vec3_map(func(int, T) = T, vec3(T)) = vec3(T).
:- func vec4_map(func(int, T) = T, vec4(T)) = vec4(T).

:- func vec3_map_corresponding(func(T, T) = T, vec3(T), vec3(T)) = vec3(T).
:- func vec4_map_corresponding(func(T, T) = T, vec4(T), vec4(T)) = vec4(T).

:- func mat4_map(func(int, int, T) = T, mat4(T)) = mat4(T).

:- func mat4_map_corresponding(func(T, T) = T, mat4(T), mat4(T)) = mat4(T).

:- func degrees_to_radians(float) = float.
:- func radians_to_degrees(float) = float.

%--------------------------------------------------%
:- implementation.
:- import_module float, math, string, list.
:- use_module exception.

%--------------------------------------------------%
% Constructors

% TODO: Is there a better way to tell mercury that this is a constant?
:- pragma memo(mat4f_identity/0).
mat4f_identity = vec4(
	vec4(1.0, 0.0, 0.0, 0.0),
	vec4(0.0, 1.0, 0.0, 0.0),
	vec4(0.0, 0.0, 1.0, 0.0),
	vec4(0.0, 0.0, 0.0, 1.0)
).

mat4f_scale(X, Y, Z) = vec4(
	vec4(X,   0.0, 0.0, 0.0),
	vec4(0.0, Y,   0.0, 0.0),
	vec4(0.0, 0.0, Z,   0.0),
	vec4(0.0, 0.0, 0.0, 1.0)
).
mat4f_scale(M, X, Y, Z) = vec4(
	vec4(X*M11, X*M12, X*M13, X*M14),
	vec4(Y*M21, Y*M22, Y*M23, Y*M24),
	vec4(Z*M31, Z*M32, Z*M33, Z*M34),
	vec4(  M41,   M42,   M43,   M44)
) :-
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	).

mat4f_translate(X, Y, Z) = vec4(
	vec4(1.0, 0.0, 0.0, X),
	vec4(0.0, 1.0, 0.0, Y),
	vec4(0.0, 0.0, 1.0, Z),
	vec4(0.0, 0.0, 0.0, 1.0)
).
mat4f_translate(M, X, Y, Z) = vec4(
	vec4(fma(X, M41, M11), fma(X, M42, M12), fma(X, M43, M13), fma(X, M44, M14)),
	vec4(fma(Y, M41, M21), fma(Y, M42, M22), fma(Y, M43, M23), fma(Y, M44, M24)),
	vec4(fma(Z, M41, M31), fma(Z, M42, M32), fma(Z, M43, M33), fma(Z, M44, M34)),
	vec4(M41, M42, M43, M44)
)
% vec4(
%	vec4(M11 + X*M41, M12 + X*M42, M13 + X*M43, M14 + X*M44),
%	vec4(M21 + Y*M41, M22 + Y*M42, M23 + Y*M43, M24 + Y*M44),
%	vec4(M31 + Z*M41, M32 + Z*M42, M33 + Z*M43, M34 + Z*M44),
%	vec4(M41,         M42,         M43,         M44)
% )
:-
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	).

mat4f_rotate_x(Radians) = vec4(
	vec4(1.0, 0.0, 0.0,  0.0),
	vec4(0.0, Cos, -Sin, 0.0),
	vec4(0.0, Sin, Cos,  0.0),
	vec4(0.0, 0.0, 0.0,  1.0)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians).
mat4f_rotate_x(M, Radians) = vec4(
	vec4(M11,                M12,                M13,                M14),
	vec4(Cos*M21 + -Sin*M31, Cos*M22 + -Sin*M32, Cos*M23 + -Sin*M33, Cos*M24 + -Sin*M34),
	vec4(Sin*M21 + Cos*M31,  Sin*M22 + Cos*M32,  Sin*M23 + Cos*M33,  Sin*M24 + Cos*M34),
	vec4(M41,                M42,                M43,                M44)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians),
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	).

mat4f_rotate_y(Radians) = vec4(
	vec4(Cos,  0.0, Sin, 0.0),
	vec4(0.0,  1.0, 0.0, 0.0),
	vec4(-Sin, 0.0, Cos, 0.0),
	vec4(0.0,  0.0, 0.0, 1.0)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians).
mat4f_rotate_y(M, Radians) = vec4(
	vec4(Cos*M11 + Sin*M31,  Cos*M12 + Sin*M32,  Cos*M13 + Sin*M33,  Cos*M14 + Sin*M34),
	vec4(M21,                M22,                M23,                M24),
	vec4(-Sin*M11 + Cos*M31, -Sin*M12 + Cos*M32, -Sin*M13 + Cos*M33, -Sin*M14 + Cos*M34),
	vec4(M41,                M42,                M43,                M44)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians),
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	).

mat4f_rotate_z(Radians) = vec4(
	vec4(Cos, -Sin, 0.0, 0.0),
	vec4(Sin, Cos,  0.0, 0.0),
	vec4(0.0, 0.0,  1.0, 0.0),
	vec4(0.0, 0.0,  0.0, 1.0)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians).
mat4f_rotate_z(M, Radians) = vec4(
	vec4(Cos*M11 + -Sin*M21, Cos*M12 + -Sin*M22, Cos*M13 + -Sin*M23, Cos*M14 + -Sin*M24),
	vec4(Sin*M11 + Cos*M21,  Sin*M12 + Cos*M22,  Sin*M13 + Cos*M23,  Sin*M14 + Cos*M24),
	vec4(M31,                M32,                M33,                M34),
	vec4(M41,                M42,                M43,                M44)
) :-
	Cos = cos(Radians),
	Sin = sin(Radians),
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	).

mat4f_frustum(L, R, B, T, N, F) = vec4(
	vec4(2.0*N/(R - L), 0.0,           (R + L)/(R - L),  0.0),
	vec4(0.0,           2.0*N/(T - B), (T + B)/(T - B),  0.0),
	vec4(0.0,           0.0,           -(F + N)/(F - N), (-2.0*F*N)/(F - N)),
	vec4(0.0,           0.0,           -1.0,             0.0)
).

mat4f_ortho(L, R, B, T, N, F) = vec4(
	vec4(2.0/(R - L), 0.0,         0.0,          -(R + L)/(R - L)),
	vec4(0.0,         2.0/(T - B), 0.0,          -(T + B)/(T - B)),
	vec4(0.0,         0.0,         -2.0/(F - N), -(F + N)/(F - N)),
	vec4(0.0,         0.0,         0.0,          1.0)
).

mat4f_perspective(YFov, Aspect, N, F) = vec4(
	vec4(A / Aspect, 0.0, 0.0,                0.0),
	vec4(0.0,        A,   0.0,                0.0),
	vec4(0.0,        0.0, -((F + N)/(F - N)), -((2.0*F*N)/(F - N))),
	vec4(0.0,        0.0, -1.0,               0.0)
) :-
	A = 1.0 / tan(YFov / 2.0).

mat4f_look_at(Eye, Center, Up) = vec4(
	vec4(Sx,  Sy,  Sz,  Sx*(-Ex) + Sy*(-Ey) + Sz*(-Ez)),
	vec4(Tx,  Ty,  Tz,  Tx*(-Ex) + Ty*(-Ey) + Tz*(-Ez)),
	vec4(-Fx, -Fy, -Fz, Fx*Ex + Fy*Ey + Fz*Ez),
	vec4(0.0, 0.0, 0.0, 0.0)
) :-
	Forward = vec3f_normalize(vec3f_sub(Center, Eye)),
	Forward = vec3(Fx, Fy, Fz),
	Side = vec3f_normalize(vec3f_cross(Forward, Up)),
	Side = vec3(Sx, Sy, Sz),
	vec3(Tx, Ty, Tz) = vec3f_cross(Side, Forward),
	Eye = vec3(Ex, Ey, Ez).

quatf_identity = vec4(0.0, 0.0, 0.0, 1.0).

%--------------------------------------------------%
% Math

% Vectors

vec3f_negate(vec3(X, Y, Z)) = vec3(-X, -Y, -Z).
vec4f_negate(vec4(X, Y, Z, W)) = vec4(-X, -Y, -Z, -W).

vec3f_add(vec3(Ax, Ay, Az), vec3(Bx, By, Bz)) =
	vec3(Ax + Bx, Ay + By, Az + Bz).
vec4f_add(vec4(Ax, Ay, Az, Aw), vec4(Bx, By, Bz, Bw)) =
	vec4(Ax + Bx, Ay + By, Az + Bz, Aw + Bw).

vec3f_add_scalar(vec3(X, Y, Z), S) = vec3(X + S, Y + S, Z + S).
vec4f_add_scalar(vec4(X, Y, Z, W), S) = vec4(X + S, Y + S, Z + S, W + S).

vec3f_sub(vec3(Ax, Ay, Az), vec3(Bx, By, Bz)) =
	vec3(Ax - Bx, Ay - By, Az - Bz).
vec4f_sub(vec4(Ax, Ay, Az, Aw), vec4(Bx, By, Bz, Bw)) =
	vec4(Ax - Bx, Ay - By, Az - Bz, Aw - Bw).

vec3f_sub_scalar(vec3(X, Y, Z), S) = vec3(X - S, Y - S, Z - S).
vec4f_sub_scalar(vec4(X, Y, Z, W), S) = vec4(X - S, Y - S, Z - S, W - S).

vec3f_dot(vec3(Ax, Ay, Az), vec3(Bx, By, Bz)) = Ax*Bx + Ay*By + Az*Bz.
vec4f_dot(vec4(Ax, Ay, Az, Aw), vec4(Bx, By, Bz, Bw)) =
	Ax*Bx + Ay*By + Az*Bz + Aw*Bw.

vec3f_cross(vec3(Ax, Ay, Az), vec3(Bx, By, Bz)) = vec3(
	Ay*Bz - Az*By,
	Az*Bx - Ax*Bz,
	Ax*By - Ay*Bx
).

vec3f_mul_scalar(vec3(X, Y, Z), F) = vec3(X*F, Y*F, Z*F).
vec4f_mul_scalar(vec4(X, Y, Z, W), F) = vec4(X*F, Y*F, Z*F, W*F).

vec3f_div_scalar(vec3(X, Y, Z), S) = vec3(X/S, Y/S, Z/S).
vec4f_div_scalar(vec4(X, Y, Z, W), S) = vec4(X/S, Y/S, Z/S, W/S).

vec3f_len(vec3(X, Y, Z)) = sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2)).
vec4f_len(vec4(X, Y, Z, W)) = sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2) + pow(W, 2)).

vec3f_normalize(V) = vec3f_div_scalar(V, vec3f_len(V)).
vec4f_normalize(V) = vec4f_div_scalar(V, vec4f_len(V)).

% Matrices

mat4f_add(A, B) = mat4_map_corresponding(func(AE, BE) = AE + BE, A, B).

mat4f_add_scalar(M, S) = mat4_map(func(_, _, E) = E + S, M).

mat4f_sub(A, B) = mat4_map_corresponding(func(AE, BE) = AE - BE, A, B).

mat4f_sub_scalar(M, S) = mat4_map(func(_, _, E) = E - S, M).

mat4f_scale(M, S) = mat4_map(func(_, _, E) = E * S, M).

% Doing this without loops is ugly, but looping with a bunch of
% recursive functions would be even uglier, not to mention having to
% allocate mid-results.
mat4f_mul(A, B) = vec4(
	vec4(
		A11*B11 + A12*B21 + A13*B31 + A14*B41,
		A11*B12 + A12*B22 + A13*B32 + A14*B42,
		A11*B13 + A12*B23 + A13*B33 + A14*B43,
		A11*B14 + A12*B24 + A13*B34 + A14*B44
	), vec4(
		A21*B11 + A22*B21 + A23*B31 + A24*B41,
		A21*B12 + A22*B22 + A23*B32 + A24*B42,
		A21*B13 + A22*B23 + A23*B33 + A24*B43,
		A21*B14 + A22*B24 + A23*B34 + A24*B44
	), vec4(
		A31*B11 + A32*B21 + A33*B31 + A34*B41,
		A31*B12 + A32*B22 + A33*B32 + A34*B42,
		A31*B13 + A32*B23 + A33*B33 + A34*B43,
		A31*B14 + A32*B24 + A33*B34 + A34*B44
	), vec4(
		A41*B11 + A42*B21 + A43*B31 + A44*B41,
		A41*B12 + A42*B22 + A43*B32 + A44*B42,
		A41*B13 + A42*B23 + A43*B33 + A44*B43,
		A41*B14 + A42*B24 + A43*B34 + A44*B44
	)
) :-
	A = vec4(
		vec4(A11, A12, A13, A14),
		vec4(A21, A22, A23, A24),
		vec4(A31, A32, A33, A34),
		vec4(A41, A42, A43, A44)
	),
	B = vec4(
		vec4(B11, B12, B13, B14),
		vec4(B21, B22, B23, B24),
		vec4(B31, B32, B33, B34),
		vec4(B41, B42, B43, B44)
	).

mat4f_mul_vec4f(M, V) = vec4(
	M11*X + M12*Y + M13*Z + M14*W,
	M21*X + M22*Y + M23*Z + M24*W,
	M31*X + M32*Y + M33*Z + M34*W,
	M41*X + M42*Y + M43*Z + M44*W
) :-
	M = vec4(
		vec4(M11, M12, M13, M14),
		vec4(M21, M22, M23, M24),
		vec4(M31, M32, M33, M34),
		vec4(M41, M42, M43, M44)
	),
	V = vec4(X, Y, Z, W).

% Quaternions

quatf_add(A, B) = vec4f_add(A, B).

quatf_sub(A, B) = vec4f_sub(A, B).

% quatf_mul(A, B) =


quatf_scale(Q, S) = vec4_map(func(_, E) = E * S, Q).


%--------------------------------------------------%
% Utils

vec3(X, Y, Z) ^ vec3_elem(I) =
	(	I = 0 -> X
	;	I = 1 -> Y
	;	I = 2 -> Z
	;	exception.throw(index_out_of_bounds(
			string.format("Index must be 0-2, got '%d'.", [i(I)])
		))
	).

vec3(X, Y, Z) ^ vec3_elem(I) := Value =
	(	I = 0 -> vec3(Value, Y, Z)
	;	I = 1 -> vec3(X, Value, Z)
	;	I = 2 -> vec3(X, Y, Value)
	;	exception.throw(index_out_of_bounds(
			string.format("Index must be 0-2, got '%d'.", [i(I)])
		))
	).

vec4(X, Y, Z, W) ^ vec4_elem(I) =
	(	I = 0 -> X
	;	I = 1 -> Y
	;	I = 2 -> Z
	;	I = 3 -> W
	;	exception.throw(index_out_of_bounds(
			string.format("Index must be 0-3, got '%d'.", [i(I)])
		))
	).

vec4(X, Y, Z, W) ^ vec4_elem(I) := Value =
	(	I = 0 -> vec4(Value, Y, Z, W)
	;	I = 1 -> vec4(X, Value, Z, W)
	;	I = 2 -> vec4(X, Y, Value, W)
	;	I = 3 -> vec4(X, Y, Z, Value)
	;	exception.throw(index_out_of_bounds(
			string.format("Index must be 0-3, got '%d'.", [i(I)])
		))
	).

M ^ mat4_elem(Y, X) = M ^ vec4_elem(Y) ^ vec4_elem(X).

M ^ mat4_elem(Y, X) := Value = M ^ vec4_elem(Y) ^ vec4_elem(X) := Value.

vec3_map(F, vec3(X, Y, Z)) = vec3(F(0, X), F(1, Y), F(2, Z)).
vec4_map(F, vec4(X, Y, Z, W)) = vec4(F(0, X), F(1, Y), F(2, Z), F(3, W)).

vec3_map_corresponding(F, vec3(Ax, Ay, Az), vec3(Bx, By, Bz)) =
	vec3(F(Ax, Bx), F(Ay, By), F(Az, Bz)).

vec4_map_corresponding(F, vec4(Ax, Ay, Az, Aw), vec4(Bx, By, Bz, Bw)) =
	vec4(F(Ax, Bx), F(Ay, By), F(Az, Bz), F(Aw, Bw)).

mat4_map(F, M) = vec4_map(func(Y, V) = vec4_map(func(X, E) = F(Y, X, E), V), M).

mat4_map_corresponding(F, A, B) =
	vec4_map_corresponding(
		func(AV, BV) = vec4_map_corresponding(F, AV, BV),
		A, B
	).

degrees_to_radians(D) = D*(pi/180.0).
radians_to_degrees(R) = R*(180.0/pi).

:- end_module gmath.
