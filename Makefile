MMC_GRADE = hlc.par.gc
#MMC_GRADE = asm_fast.gc.debug.stseg

MMC_INFER = --infer-all

MMC_OPTIMIZATION = --introduce-accumulators --intermodule-optimization

MMC_WARNINGS = --warn-unused-imports --warn-non-contiguous-foreign-procs --warn-duplicate-calls --warn-non-tail-recursion self-and-mutual --warn-dead-procs --warn-dead-preds --warn-unknown-format-calls --inform-ite-instead-of-switch --inform-incomplete-switch --warn-suspicious-foreign-procs --warn-suspicious-foreign-code --inform-suboptimal-packing --output-compile-error-lines 30

MMC_OPTIONS = --allow-stubs --track-flags --env-type windows

MMC_CFLAGS = --cflags "-std=c11" --cflag "-Dmain=SDL_main"

MMC_LDFLAGS = --ld-flag "-mwindows"

MMC_C_INCLUDES = --c-include-directory "include/" -lSDL2 -lopengl32

MMC = C:/mercury/bin/mmc --grade $(MMC_GRADE) $(MMC_INFER) $(MMC_OPTIMIZATION) $(MMC_WARNINGS) $(MMC_OPTIONS) $(MMC_CFLAGS) $(MMC_LDFLAGS) $(MMC_C_INCLUDES)

glad.o: c/glad.c
	gcc -o glad.o -c c/glad.c

gltest: glad.o *.m
	$(MMC) --link-object glad.o --make gltest

