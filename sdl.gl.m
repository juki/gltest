%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.gl.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module sdl.gl.

%--------------------------------------------------%
:- interface.
:- import_module bool, list.
:- use_module io, maybe.

:- use_module sdl.window.

%--------------------------------------------------%
% Attributes

:- type attribute
	--->	red_size
	;	green_size
	;	blue_size
	;	alpha_size
	;	buffer_size
	;	doublebuffer
	;	depth_size
	;	stencil_size
	;	accum_red_size
	;	accum_green_size
	;	accum_blue_size
	;	accum_alpha_size
	;	stereo
	;	multisamplebuffers
	;	multisamplesamples
	;	accelerated_visual
	;	retained_backing
	;	context_major_version
	;	context_minor_version
	;	context_egl
	;	share_with_current_context
	;	framebuffer_srgb_capable
	;	context_release_behavior
	;	context_reset_notification
	;	context_no_error.

	% set_attribute(Attr, Value, Success, Result, !IO):
	%
	% Set an attribute to an integer value.
	%
:- pred set_attribute(attribute, int, bool, io.io, io.io).
:- mode set_attribute(in, in, out, di, uo) is det.

:- type context_profile
	--->	core
	;	compatibility
	;	es.

:- pred set_context_profile_mask(list(context_profile), bool, io.io, io.io).
:- mode set_context_profile_mask(in, out, di, uo) is det.

:- type context_flag
	--->	debug
	;	forward_compatible
	;	robust_access
	;	reset_isolation.

:- pred set_context_flags(list(context_flag), bool, io.io, io.io).
:- mode set_context_flags(in, out, di, uo) is det.

%--------------------------------------------------%
% Loading GL

	% Load the default OpenGL library.
:- pred load_default_library(maybe.maybe_error::out, io.io::di, io.io::uo) is det.

	% Load an OpenGL library from a given path.
:- pred load_library(string, maybe.maybe_error, io.io, io.io).
:- mode load_library(in, out, di, uo) is det.

	% Unload the current OpenGL library.
:- pred unload_library(io.io::di, io.io::uo) is det.

%--------------------------------------------------%
% Contexts

	% A GL context.
:- type context.

:- pred create_context(
	sdl.window.window::in, context::out, io.io::di, io.io::uo
) is det.

:- pred make_current(
	sdl.window.window::in, context::in, io.io::di, io.io::uo
) is det.

:- pred get_current_window(sdl.window.window::out, io.io::di, io.io::uo) is det.

:- pred get_current_context(context::out, io.io::di, io.io::uo) is det.

:- pred delete_context(context::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
% Drawing

:- pred get_drawable_size(
	sdl.window.window::in, int::out, int::out, io.io::di, io.io::uo
) is det.

:- type swap_interval
	--->	immediate
	;	vsync
	;	adaptive_vsync.

:- pred set_swap_interval(
	swap_interval::in, maybe.maybe_error::out, io.io::di, io.io::uo
) is det.

:- pred get_swap_interval(swap_interval::out, io.io::di, io.io::uo) is det.

:- pred swap_window(sdl.window.window::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.

%--------------------------------------------------%
% Attributes

:- pragma foreign_enum("C", attribute/0, [
	red_size - "SDL_GL_RED_SIZE",
	green_size - "SDL_GL_GREEN_SIZE",
	blue_size - "SDL_GL_BLUE_SIZE",
	alpha_size - "SDL_GL_ALPHA_SIZE",
	buffer_size - "SDL_GL_BUFFER_SIZE",
	doublebuffer - "SDL_GL_DOUBLEBUFFER",
	depth_size - "SDL_GL_DEPTH_SIZE",
	stencil_size - "SDL_GL_STENCIL_SIZE",
	accum_red_size - "SDL_GL_ACCUM_RED_SIZE",
	accum_green_size - "SDL_GL_ACCUM_GREEN_SIZE",
	accum_blue_size - "SDL_GL_ACCUM_BLUE_SIZE",
	accum_alpha_size - "SDL_GL_ACCUM_ALPHA_SIZE",
	stereo - "SDL_GL_STEREO",
	multisamplebuffers - "SDL_GL_MULTISAMPLEBUFFERS",
	multisamplesamples - "SDL_GL_MULTISAMPLESAMPLES",
	accelerated_visual - "SDL_GL_ACCELERATED_VISUAL",
	retained_backing - "SDL_GL_RETAINED_BACKING",
	context_major_version - "SDL_GL_CONTEXT_MAJOR_VERSION",
	context_minor_version - "SDL_GL_CONTEXT_MINOR_VERSION",
	context_egl - "SDL_GL_CONTEXT_EGL",
	share_with_current_context - "SDL_GL_SHARE_WITH_CURRENT_CONTEXT",
	framebuffer_srgb_capable - "SDL_GL_FRAMEBUFFER_SRGB_CAPABLE",
	context_release_behavior - "SDL_GL_CONTEXT_RELEASE_BEHAVIOR",
	context_reset_notification - "SDL_GL_CONTEXT_RESET_NOTIFICATION",
	context_no_error - "SDL_GL_CONTEXT_NO_ERROR"
]).

:- pragma foreign_proc("C",
	set_attribute(Attribute::in, Value::in, Success::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"
	if (SDL_GL_SetAttribute(Attribute, Value) == 0) {
		Success = MR_YES;
	} else {
		Success = MR_NO;
	}
	IO1 = IO0;
	"
).

:- pragma foreign_enum("C", context_profile/0, [
	core - "SDL_GL_CONTEXT_PROFILE_CORE",
	compatibility - "SDL_GL_CONTEXT_PROFILE_COMPATIBILITY",
	es - "SDL_GL_CONTEXT_PROFILE_ES"
]).

:- pragma foreign_proc("C",
	set_context_profile_mask(Profile::in, Success::out, IO0::di, IO1::uo),
	[promise_pure],
	"
	uint32_t flags = GLTEST_SDL_CombineFlags(Profile);
	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, flags) == 0) {
		Success = MR_YES;
	} else {
		Success = MR_NO;
	}
	IO1 = IO0;
	"
).

:- pragma foreign_enum("C", context_flag/0, [
	debug - "SDL_GL_CONTEXT_DEBUG_FLAG",
	forward_compatible - "SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG",
	robust_access - "SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG",
	reset_isolation - "SDL_GL_CONTEXT_RESET_ISOLATION_FLAG"
]).

:- pragma foreign_proc("C",
	set_context_flags(Flags::in, Success::out, IO0::di, IO1::uo),
	[promise_pure],
	"
	uint32_t flags = GLTEST_SDL_CombineFlags(Flags);
	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, flags) == 0) {
		Success = MR_YES;
	} else {
		Success = MR_NO;
	}
	IO1 = IO0;
	"
).


%--------------------------------------------------%
% Loading GL

:- pragma promise_pure(load_default_library/3).
load_default_library(Result, !IO) :-
	(	impure sdl_gl_load_default_library
	->	Result = maybe.ok
	;	Result = maybe.error(sdl.get_error(!.IO, !:IO))
	).

:- impure pred sdl_gl_load_default_library is semidet.
:- pragma foreign_proc("C", sdl_gl_load_default_library,
	[will_not_call_mercury],
	"
	if (SDL_GL_LoadLibrary(NULL) == 0) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}"
).

:- pragma promise_pure(load_library/4).
load_library(Path, Result, !IO) :-
	(	impure sdl_gl_load_library(Path)
	->	Result = maybe.ok
	;	Result = maybe.error(sdl.get_error(!.IO, !:IO))
	).

:- impure pred sdl_gl_load_library(string::in) is semidet.
:- pragma foreign_proc("C", sdl_gl_load_library(Path::in),
	[will_not_call_mercury],
	"
	if (SDL_GL_LoadLibrary(Path) == 0) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}"
).

:- pragma foreign_proc("C", unload_library(IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"SDL_GL_UnloadLibrary(); IO1 = IO0;"
).

%--------------------------------------------------%
% Contexts

:- pragma foreign_type("C", context, "SDL_GLContext").

:- pragma foreign_proc("C",
	create_context(Window::in, Context::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"Context = SDL_GL_CreateContext(Window); IO1 = IO0;"
).

:- pragma foreign_proc("C",
	make_current(Window::in, Context::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"SDL_GL_MakeCurrent(Window, Context); IO1 = IO0;"
).

:- pragma foreign_proc("C",
	get_current_window(Window::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"Window = SDL_GL_GetCurrentWindow(); IO1 = IO0;"
).

:- pragma foreign_proc("C",
	get_current_context(Context::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"Context = SDL_GL_GetCurrentContext(); IO1 = IO0;"
).

:- pragma foreign_proc("C",
	delete_context(Context::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"SDL_GL_DeleteContext(Context); IO1 = IO0;"
).

%--------------------------------------------------%
% Drawing

:- pragma foreign_proc("C",
	get_drawable_size(Window::in, W::out, H::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	int w, h;
	SDL_GL_GetDrawableSize(Window, &w, &h);
	W = w;
	H = h;
	IO1 = IO0;
	"
).

:- pragma foreign_enum("C", swap_interval/0, [
	immediate - "0",
	vsync - "1",
	adaptive_vsync - "-1"
]).

:- pragma promise_pure(set_swap_interval/4).
set_swap_interval(Interval, Result, !IO) :-
	(	impure sdl_gl_set_swap_interval(Interval)
	->	Result = maybe.ok
	;	Result = maybe.error(sdl.get_error(!.IO, !:IO))
	).

:- impure pred sdl_gl_set_swap_interval(swap_interval::in) is semidet.
:- pragma foreign_proc("C", sdl_gl_set_swap_interval(Interval::in),
	[will_not_call_mercury],
	"
	if (SDL_GL_SetSwapInterval(Interval) == 0) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma foreign_proc("C", get_swap_interval(Interval::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"Interval = SDL_GL_GetSwapInterval(); IO1 = IO0;"
).

:- pragma foreign_proc("C", swap_window(Window::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"SDL_GL_SwapWindow(Window); IO1 = IO0;"
).

:- end_module sdl.gl.
