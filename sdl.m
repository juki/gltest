%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module sdl.

%--------------------------------------------------%
:- interface.
:- import_module list.
:- use_module io, maybe.

:- include_module
	sdl.audio,
	sdl.mouse,
	sdl.keyboard,
	sdl.window,
	sdl.display,
	sdl.gl, % Depends on sdl.window.
	sdl.events. % Depends on sdl.audio, sdl.mouse, sdl.keyboard,
			% sdl.window, sdl.display.

%--------------------------------------------------%
% Initializing and quitting SDL.

	% SDL2 initialization flags.
	%
	% Only supported modules of SDL are allowed to be initialized
	% (ie. no sensors, controllers, etc.).
	%
:- type init_flag
	--->	timer
	;	audio
	;	video
	;	events.

:- type init_flags == list(init_flag).

	% Initialize SDL.
:- pred init(init_flags::in, maybe.maybe_error::out, io.io::di, io.io::uo) is det.

	% Quit SDL.
:- pred quit(io.io::di, io.io::uo) is det.

:- pred call_with_sdl(
	init_flags::in,
	pred(T, io.io, io.io)::pred(out, di, uo) is det,
	maybe.maybe_error(T)::out,
	io.io::di, io.io::uo
) is det.

%--------------------------------------------------%
% Errors

	% get_error(ErrorString, !IO):
	%
	% Retrieve the latest error string from SDL.
	%
:- pred get_error(string::out, io.io::di, io.io::uo) is det.

:- func get_error(io.io::di, io.io::uo) = (string::out) is det.

%--------------------------------------------------%
% Time

:- type ticks == uint32.

:- pred get_ticks(ticks::out, io.io::di, io.io::uo) is det.

:- func get_ticks(io.io::di, io.io::uo) = (ticks::out) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").

%--------------------------------------------------%
% Flags

:- pragma foreign_decl("C", "uint32_t GLTEST_SDL_CombineFlags(MR_Word flags);").
:- pragma foreign_code(
	"C",
	"
	uint32_t GLTEST_SDL_CombineFlags(MR_Word list_of_flags) {
		uint32_t flags = 0;
		MR_Word remaining_flags = list_of_flags;
		while (!MR_list_is_empty(remaining_flags)) {
			uint64_t flag = MR_word_to_uint64(MR_list_head(remaining_flags));
			flags |= flag;
			remaining_flags = MR_list_tail(remaining_flags);
		}
		return flags;
	}
	"
).

%--------------------------------------------------%
% Initializing and quitting SDL.

:- pragma foreign_enum(
	"C",
	init_flag/0,
	[
		timer - "SDL_INIT_TIMER",
		audio - "SDL_INIT_AUDIO",
		video - "SDL_INIT_VIDEO",
		events - "SDL_INIT_EVENTS"
	]
).

:- pragma promise_pure(init/4).
init(Flags, Result, !IO) :-
	impure InitValue = sdl_init(Flags),
	Result = (InitValue = 0 -> maybe.ok ; maybe.error(get_error(!.IO, !:IO))).

:- impure func sdl_init(init_flags) = int is det.
:- pragma foreign_proc("C", sdl_init(Flags::in) = (Value::out),
	[],
	"Value = SDL_Init(GLTEST_SDL_CombineFlags(Flags));"
).

:- pragma foreign_proc("C", quit(IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"
	SDL_Quit();
	IO1 = IO0;
	"
).

call_with_sdl(Flags, P, Result, !IO) :-
	init(Flags, InitResult, !IO),
	(	InitResult = maybe.ok,
		call(P, Value, !IO),
		Result = maybe.ok(Value),
		quit(!IO)
	;	InitResult = maybe.error(ErrStr),
		Result = maybe.error(ErrStr)
	).

%--------------------------------------------------%
% Errors

:- pragma foreign_proc("C", get_error(ErrStr::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"
	const char* sdl_err = SDL_GetError();
	ErrStr = malloc(sizeof(char[strlen(sdl_err) + 1]));
	strcpy(ErrStr, sdl_err);
	IO1 = IO0;
	"
).

get_error(!IO) = ErrStr :-
	get_error(ErrStr, !IO).


%--------------------------------------------------%
% Time

:- pragma inline(get_ticks/3).
:- pragma foreign_proc("C", get_ticks(Ticks::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	Ticks = SDL_GetTicks();
	IO1 = IO0;
	"
).

get_ticks(!IO) = Ticks :- get_ticks(Ticks, !IO).

:- end_module sdl.
