%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.mouse.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%
:- module sdl.mouse.

%--------------------------------------------------%
:- interface.
:- import_module bool.

:- type mouse_button
	--->	left
	;	middle
	;	right
	;	x1
	;	x2.

:- type mouse_wheel_direction
	--->	normal
	;	flipped.

:- type mouse_button_state == uint32.

	% mouse_button_state(State, Left, Middle, Right, X1, X2):
	%
	% Unpack the state of mouse buttons. Each of `Left`, `Middle`,
	% `Right`, `X1` and `X2` are `yes` or `no` to indicate whether
	% that button is currently pressed.
	%
:- pred mouse_button_state(mouse_button_state, bool, bool, bool, bool, bool).
:- mode mouse_button_state(in, out, out, out, out, out) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").

:- pragma foreign_enum("C", mouse_button/0, [
	left - "SDL_BUTTON_LEFT",
	middle - "SDL_BUTTON_MIDDLE",
	right - "SDL_BUTTON_RIGHT",
	x1 - "SDL_BUTTON_X1",
	x2 - "SDL_BUTTON_X2"
]).

:- pragma foreign_enum("C", mouse_wheel_direction/0, [
	normal - "SDL_MOUSEWHEEL_NORMAL",
	flipped - "SDL_MOUSEWHEEL_FLIPPED"
]).

:- pragma foreign_proc("C",
	mouse_button_state(BState::in, Left::out, Middle::out, Right::out, X1::out, X2::out),
	[promise_pure],
	"
	Left = (BState & SDL_BUTTON_LMASK)  ? MR_YES : MR_NO;
	Middle = (BState & SDL_BUTTON_MMASK)  ? MR_YES : MR_NO;
	Right = (BState & SDL_BUTTON_RMASK)  ? MR_YES : MR_NO;
	X1 = (BState & SDL_BUTTON_X1MASK) ? MR_YES : MR_NO;
	X2 = (BState & SDL_BUTTON_X2MASK) ? MR_YES : MR_NO;
	"
).

:- end_module sdl.mouse.
