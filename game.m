%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: game.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module game.

%--------------------------------------------------%
:- interface.
:- import_module store.
:- use_module io.
:- use_module
	sdl,
	sdl.window,
	sdl.gl,
	sdl.events,
	sdl.keyboard.
:- use_module gl.
:- use_module shader.
:- use_module gmath.

:- type game
	--->	game(
			prev_frame_ms                :: io_mutvar(sdl.ticks),
			exp_program                  :: shader.program,
			exp_vao                      :: gl.gl_uint,
			exp_uniform_location         :: gl.uniform_location,
			exp_transform_location       :: gl.uniform_location,
			exp_projection_mat_location  :: gl.uniform_location,
			projection_matrix            :: io_mutvar(gmath.mat4(float)),
			frame_counter                :: io_mutvar(int)
		).

:- pred init(game, io.io, io.io).
:- mode init(out, di, uo) is det.

:- pred destroy(game, io.io, io.io).
:- mode destroy(in, di, uo) is det.

:- pred idle(sdl.window.window, sdl.gl.context, game, io.io, io.io).
:- mode idle(in, in, in, di, uo) is det.

:- pred key_up(
	sdl.events.button_state::in,
	uint8::in,
	sdl.keyboard.scancode::in,
	sdl.keyboard.keymods::in,
	game::in,
	io.io::di, io.io::uo
) is det.

:- pred viewport_size_changed(int, int, game, io.io, io.io).
:- mode viewport_size_changed(in, in, in, di, uo) is det.

%--------------------------------------------------%
:- implementation.
:- import_module uint8, uint32, int, float, list, string.
:- use_module require, maybe.
:- use_module stbi.

:- func ms_per_update = sdl.ticks.
ms_per_update = 8u32.

:- func ms_per_update_float = float.
ms_per_update_float = float(cast_to_int(ms_per_update)).

:- pred build_program_or_die(shader.shader_sources, shader.program, io.io, io.io).
:- mode build_program_or_die(in, out, di, uo) is det.
build_program_or_die(Sources, Program, !IO) :-
	shader.build_program(Sources, MaybeProgram, !IO),
	(	MaybeProgram = maybe.ok(Program)
	;	MaybeProgram = maybe.error(Err),
		require.unexpected($module, Err)
	).

init(Game, !IO) :-
	build_program_or_die(test_shaders, Program, !IO),
	shader.uniform_location(Program, "color", MaybeUniform, !IO),
	(	MaybeUniform = maybe.yes(Uniform)
	;	MaybeUniform = maybe.no,
		require.unexpected($module, "Failed to get uniform location.")
	),
	shader.uniform_location(Program, "transform", MaybeTransform, !IO),
	(	MaybeTransform = maybe.yes(Transform)
	;	MaybeTransform = maybe.no,
		require.unexpected($module, "Failed to get uniform location.")
	),
	shader.uniform_location(Program, "projection", MaybeProjectionLocation, !IO),
	(	MaybeProjectionLocation = maybe.yes(ProjectionLocation)
	;	MaybeProjectionLocation = maybe.no,
		require.unexpected($module, "Failed to get uniform location.")
	),
	exp_setup_vao(VAO, !IO),
	sdl.get_ticks(Ticks, !IO),
	new_mutvar(Ticks, PrevFrameMsVar, !IO),
	new_mutvar(gmath.mat4f_identity, ProjectionVar, !IO),
	new_mutvar(0, FrameCounterVar, !IO),
	Game = game(
		PrevFrameMsVar,
		Program,
		VAO,
		Uniform,
		Transform,
		ProjectionLocation,
		ProjectionVar,
		FrameCounterVar
	),
	shader.use_program(Game ^ exp_program, !IO),
	shader.uniform_vec3(
		Game ^ exp_uniform_location,
		gmath.vec3(0.0, 0.0, 0.8),
		!IO
	).

destroy(Game, !IO) :-
	shader.delete_program(Game ^ exp_program, !IO),
	delete_vao(Game ^ exp_vao, !IO).

idle(Window, GLContext, Game, !IO) :-
	sdl.get_ticks(Current, !IO),
	get_mutvar(Game ^ game.prev_frame_ms, Previous, !IO),
	idle_inner(Current - Previous, Window, GLContext, Game, !IO),
	set_mutvar(Game ^ game.prev_frame_ms, Current, !IO).

:- pred idle_inner(sdl.ticks, sdl.window.window, sdl.gl.context, game, io.io, io.io).
:- mode idle_inner(in, in, in, in, di, uo) is det.
idle_inner(Lag, Window, GLContext, Game, !IO) :-
	(	Lag >= ms_per_update
	->	update(Game, !IO),
		idle_inner(Lag - ms_per_update, Window, GLContext, Game, !IO)
	;	RemainingLag = float(cast_to_int(Lag)) / ms_per_update_float,
		render(RemainingLag, Window, GLContext, Game, !IO)
	).

key_up(_BState, _Repeat, Scancode, Mods, _Game, !IO) :-
	(	Scancode = sdl.keyboard.scancode_q,
		sdl.keyboard.has_keymod(Mods, sdl.keyboard.kmod_ctrl)
	->	io.format("Pushing quit event.\n", [], !IO),
		sdl.events.push_quit_event(!IO)
	;	true
	).

viewport_size_changed(W, H, Game, !IO) :-
	ProjectionMatrix = gmath.mat4f_ortho(0.0, float(W), float(H), 0.0, -1.0, 1.0),
	set_mutvar(Game ^ projection_matrix, ProjectionMatrix, !IO),
	shader.use_program(Game ^ exp_program, !IO),
	shader.uniform_mat4(
		Game ^ exp_projection_mat_location,
		ProjectionMatrix,
		!IO
	).

%--------------------------------------------------%
% Updating

:- pred update(game, io.io, io.io).
:- mode update(in, di, uo) is det.
update(_Game, !IO).

%--------------------------------------------------%
% Rendering

:- pred render(float, sdl.window.window, sdl.gl.context, game, io.io, io.io).
:- mode render(in, in, in, in, di, uo) is det.
render(Lag, Window, _GLContext, Game, !IO) :-
	get_mutvar(Game ^ frame_counter, FrameCount, !IO),
	(	FrameCount rem 60 = 0
	->	sdl.get_ticks(Ticks, !IO),
		io.format(
			"Frame %d: %d (%f)\n",
			[i(FrameCount), i(cast_to_int(Ticks)), f(Lag)],
			!IO
		)
	;	true
	),
	set_mutvar(Game ^ frame_counter, FrameCount + 1, !IO),
	gl.clear_with_color(0.9, 0.4, 0.4, 1.0, !IO),
	shader.use_program(Game ^ exp_program, !IO),
	shader.uniform_mat4(
		Game ^ exp_transform_location,
		gmath.mat4f_translate(
			gmath.mat4f_scale(1.0, 1.0, 1.0),
			100.0, 100.0, 0.0
		),
		!IO
	),
	draw_test(Game ^ exp_vao, !IO),
	sdl.gl.swap_window(Window, !IO).

%--------------------------------------------------%
% Experimentation

:- func test_shaders = shader.shader_sources.
test_shaders = [
	shader.shader_source(gl.vertex_shader, "
		#version 330 core
		layout (location = 0) in vec3 aPos;

		uniform mat4 projection;
		uniform mat4 transform;

		void main() {
			gl_Position = projection * transform * vec4(aPos, 1.0);
		}
	"),
	shader.shader_source(gl.fragment_shader, "
		#version 330 core
		out vec4 FragColor;

		uniform vec3 color;

		void main() {
			FragColor = vec4(color, 1.0f);
		}
	")
].

:- pragma foreign_decl("C", "#include ""glad/glad.h""").

:- pred exp_setup_vao(gl.gl_uint::out, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", exp_setup_vao(VAOOut::out, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	//float vertices[] = {
	//	0.5f,  0.5f, 0.0f,
	//	0.5f, -0.5f, 0.0f,
	//	-0.5f, -0.5f, 0.0f,
	//	-0.5f,  0.5f, 0.0f
	//};
	float vertices[] = {
		300.0f,  150.0f, 0.0f,
		300.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f,  150.0f, 0.0f
	};

	unsigned int indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	unsigned int VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	unsigned int VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	unsigned int EBO;
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	VAOOut = VAO;
	IO1 = IO0;
	"
).

:- pred delete_vao(gl.gl_uint::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", delete_vao(VAO::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glDeleteVertexArrays(1, &VAO);
	IO1 = IO0;
	"
).

:- pred draw_test(gl.gl_uint::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", draw_test(VAO::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*) 0);
	IO1 = IO0;
	"
).

:- end_module game.
