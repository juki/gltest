%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: shader.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
% This module manages shader programs for the game.
%
% This is mainly a safer wrapper around GL functions. The exported
% predicates take an IO state pair as a way of providing some purity.
%
%--------------------------------------------------%

:- module shader.

%--------------------------------------------------%
:- interface.
:- import_module list.
:- use_module io, maybe.
:- use_module gmath.
:- use_module gl.

:- type shader_source
	--->	shader_source(gl.shader_type, string).

:- type shader_sources == list(shader_source).

:- type program.

:- pred build_program(shader_sources, maybe.maybe_error(program), io.io, io.io).
:- mode build_program(in, out, di, uo) is det.

:- pred use_program(program::in, io.io::di, io.io::uo) is det.

:- pred delete_program(program::in, io.io::di, io.io::uo) is det.

:- pred uniform_location(program, string, maybe.maybe(gl.uniform_location), io.io, io.io).
:- mode uniform_location(in, in, out, di, uo) is det.

:- pred uniform_1f(gl.uniform_location::in, float::in, io.io::di, io.io::uo) is det.
:- pred uniform_2f(gl.uniform_location::in, float::in, float::in, io.io::di, io.io::uo) is det.
:- pred uniform_3f(gl.uniform_location::in, float::in, float::in, float::in, io.io::di, io.io::uo) is det.
:- pred uniform_4f(gl.uniform_location::in, float::in, float::in, float::in, float::in, io.io::di, io.io::uo) is det.

:- pred uniform_1i(gl.uniform_location::in, int::in, io.io::di, io.io::uo) is det.
:- pred uniform_2i(gl.uniform_location::in, int::in, int::in, io.io::di, io.io::uo) is det.
:- pred uniform_3i(gl.uniform_location::in, int::in, int::in, int::in, io.io::di, io.io::uo) is det.
:- pred uniform_4i(gl.uniform_location::in, int::in, int::in, int::in, int::in, io.io::di, io.io::uo) is det.

:- pred uniform_vec3(gl.uniform_location::in, gmath.vec3(float)::in, io.io::di, io.io::uo) is det.
:- pred uniform_vec4(gl.uniform_location::in, gmath.vec4(float)::in, io.io::di, io.io::uo) is det.

:- pred uniform_mat4(gl.uniform_location::in, gmath.mat4(float)::in, io.io::di, io.io::uo) is det.

% :- pred uniform_vec3(gl.uniform_location::in, gmath.vec3::array_ui, io.io::di, io.io::uo) is det.
% :- pred uniform_vec4(gl.uniform_location::in, gmath.vec4::array_ui, io.io::di, io.io::uo) is det.

% :- pred uniform_mat4(gl.uniform_location::in, gmath.mat4::array2d_ui, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.

:- type program
	--->	program(
			id::gl.program_id
		).

:- pred compile_shader_source(
	shader_source::in,
	maybe.maybe_error(gl.shader_id)::out,
	io.io::di, io.io::uo
) is det.
:- pragma promise_pure(compile_shader_source/4).
compile_shader_source(shader_source(Type, Source), MaybeShader, !IO) :-
	(	impure gl.create_shader(Type, ShaderID)
	->	impure gl.shader_source(ShaderID, Source),
		impure gl.compile_shader(ShaderID),
		(	impure gl.shader_compile_error(ShaderID, ErrStr)
		->	MaybeShader = maybe.error(ErrStr)
		;	MaybeShader = maybe.ok(ShaderID)
		)
	;	MaybeShader = maybe.error("Failed to create a shader.")
	).

:- impure pred delete_shaders(list(gl.shader_id)::in) is det.
delete_shaders([]).
delete_shaders([ShaderID | MoreShaderIDs]) :-
	impure gl.delete_shader(ShaderID),
	impure delete_shaders(MoreShaderIDs).

:- func create_shaders(shader_sources, list(gl.shader_id), io.io, io.io) = maybe.maybe_error(list(gl.shader_id)).
:- mode create_shaders(in, in, di, uo) = out is det.
:- pragma promise_pure(create_shaders/4).
create_shaders([], Acc, !IO) = maybe.ok(Acc).
create_shaders([SS | More], Acc, !IO) = ShaderIDs :-
	compile_shader_source(SS, MaybeShaderID, !IO),
	(	MaybeShaderID = maybe.ok(ShaderID),
		ShaderIDs = create_shaders(More, [ShaderID | Acc], !.IO, !:IO)
	;	MaybeShaderID = maybe.error(ErrStr),
		ShaderIDs = maybe.error(ErrStr),
		% Delete all shaders that were already compiled.
		impure delete_shaders(Acc)
	).

:- pred link_shaders(list(gl.shader_id), gl.program_id, maybe.maybe_error, io.io, io.io).
:- mode link_shaders(in, in, out, di, uo) is det.
:- pragma promise_pure(link_shaders/5).
link_shaders([], ProgramID, Result, !IO) :-
	impure gl.link_program(ProgramID),
	(	impure gl.program_link_error(ProgramID, ErrStr)
	->	Result = maybe.error(ErrStr)
	;	Result = maybe.ok
	).
link_shaders([ID | MoreIDs], ProgramID, Result, !IO) :-
	impure gl.attach_shader(ProgramID, ID),
	link_shaders(MoreIDs, ProgramID, Result, !IO).

:- pragma promise_pure(build_program/4).
build_program(ShaderSources, MaybeProgram, !IO) :-
	(	impure gl.create_program(ProgramID)
	->	MaybeShaderIDs = create_shaders(ShaderSources, [], !.IO, !:IO),
		(	MaybeShaderIDs = maybe.ok(ShaderIDs),
			link_shaders(ShaderIDs, ProgramID, Result, !IO),
			(	Result = maybe.ok,
				MaybeProgram = maybe.ok(program(ProgramID))
			;	Result = maybe.error(ErrStr),
				MaybeProgram = maybe.error(ErrStr)
			),
			impure delete_shaders(ShaderIDs)
		;	MaybeShaderIDs = maybe.error(ErrStr),
			MaybeProgram = maybe.error(ErrStr)
		)
	;	MaybeProgram = maybe.error("Failed to create a program.")
	).

:- pragma promise_pure(use_program/3).
use_program(Program, !IO) :-
	impure gl.use_program(Program ^ id).

:- pragma promise_pure(delete_program/3).
delete_program(Program, !IO) :-
	impure gl.delete_program(Program ^ id).

%--------------------------------------------------%
% Uniforms

	% The OpenGL shader program won't be modified after the
	% program(id) object has been created, so this is pure.
:- pragma promise_pure(uniform_location/5).
uniform_location(Program, Name, MaybeLocation, !IO) :-
	(	impure Location = gl.get_uniform_location(Program ^ id, Name)
	->	MaybeLocation = maybe.yes(Location)
	;	MaybeLocation = maybe.no
	).

:- pragma promise_pure(uniform_1f/4).
uniform_1f(Location, V1, !IO) :-
	impure gl.uniform_1f(Location, V1).
:- pragma promise_pure(uniform_2f/5).
uniform_2f(Location, V1, V2, !IO) :-
	impure gl.uniform_2f(Location, V1, V2).
:- pragma promise_pure(uniform_3f/6).
uniform_3f(Location, V1, V2, V3, !IO) :-
	impure gl.uniform_3f(Location, V1, V2, V3).
:- pragma promise_pure(uniform_4f/7).
uniform_4f(Location, V1, V2, V3, V4, !IO) :-
	impure gl.uniform_4f(Location, V1, V2, V3, V4).

:- pragma promise_pure(uniform_1i/4).
uniform_1i(Location, V1, !IO) :-
	impure gl.uniform_1i(Location, V1).
:- pragma promise_pure(uniform_2i/5).
uniform_2i(Location, V1, V2, !IO) :-
	impure gl.uniform_2i(Location, V1, V2).
:- pragma promise_pure(uniform_3i/6).
uniform_3i(Location, V1, V2, V3, !IO) :-
	impure gl.uniform_3i(Location, V1, V2, V3).
:- pragma promise_pure(uniform_4i/7).
uniform_4i(Location, V1, V2, V3, V4, !IO) :-
	impure gl.uniform_4i(Location, V1, V2, V3, V4).

:- pragma promise_pure(uniform_vec3/4).
uniform_vec3(Location, gmath.vec3(X, Y, Z), !IO) :-
	impure gl.uniform_3f(Location, X, Y, Z).

:- pragma promise_pure(uniform_vec4/4).
uniform_vec4(Location, gmath.vec4(X, Y, Z, W), !IO) :-
	impure gl.uniform_4f(Location, X, Y, Z, W).

:- pragma promise_pure(uniform_mat4/4).
uniform_mat4(Location, M, !IO) :-
	M = gmath.vec4(
		gmath.vec4(M11, M12, M13, M14),
		gmath.vec4(M21, M22, M23, M24),
		gmath.vec4(M31, M32, M33, M34),
		gmath.vec4(M41, M42, M43, M44)
	),
	impure gl.uniform_mat4(
		Location,
		M11, M21, M31, M41,
		M12, M22, M32, M42,
		M13, M23, M33, M43,
		M14, M24, M34, M44
	).

%--------------------------------------------------%
:- end_module shader.
