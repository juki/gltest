%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: glad.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module glad.

%--------------------------------------------------%
:- interface.
:- use_module io, maybe.

:- pred load_gl(maybe.maybe_error::out, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").
:- pragma foreign_decl("C", "#include ""glad/glad.h""").

:- pragma promise_pure(load_gl/3).
load_gl(Result, !IO) :-
	(	impure load_gl
	->	Result = maybe.ok
	;	Result = maybe.error("Glad failed to load GL.")
	).

:- impure pred load_gl is semidet.
:- pragma foreign_proc("C", load_gl,
	[will_not_call_mercury],
	"
	if (gladLoadGLLoader(SDL_GL_GetProcAddress)) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- end_module glad.
