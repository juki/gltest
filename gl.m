%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: gl.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
% This module provides foreign procs for working with OpenGL. Notice
% that this isn't a wrapper for OpenGL, but rather just a collection
% of preds needed in the game, which may or may not correspond 1-to-1
% with OpenGL functions.
%
% The foreign procs in this module are all marked as thread safe,
% although they really aren't. GL functions can only be called from
% the main thread, so if they are getting called from some other
% thread, something is very wrong already and mutexes aren't going to
% help.
%
%--------------------------------------------------%

:- module gl.

%--------------------------------------------------%
:- interface.
:- use_module io.

%--------------------------------------------------%
% Types

:- type gl_int.
:- type gl_uint.

%--------------------------------------------------%
% Viewport

:- pred viewport(int::in, int::in, int::in, int::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
% Clearing

:- pred clear_color(float::in, float::in, float::in, float::in, io.io::di, io.io::uo) is det.

:- pred clear(io.io::di, io.io::uo) is det.

:- pred clear_with_color(float::in, float::in, float::in, float::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
% Shaders / programs

:- type shader_type
	--->	vertex_shader
	;	fragment_shader.

:- type shader_id == gl_uint.
:- type program_id == gl_uint.

:- impure pred create_shader(shader_type::in, shader_id::out) is semidet.

:- impure pred shader_source(shader_id::in, string::in) is det.

:- impure pred compile_shader(shader_id::in) is det.

	% Get shader compilation error message. Succeeds if the
	% compilation failed.
:- impure pred shader_compile_error(shader_id::in, string::out) is semidet.

:- impure pred delete_shader(shader_id::in) is det.

:- impure pred create_program(program_id::out) is semidet.

:- impure pred use_program(program_id::in) is det.

:- impure pred delete_program(program_id::in) is det.

:- impure pred attach_shader(program_id::in, shader_id::in) is det.

:- impure pred link_program(program_id::in) is det.

	% Get shader compilation error message. Succeeds if the
	% compilation failed.
:- impure pred program_link_error(program_id::in, string::out) is semidet.

% Uniforms

:- type uniform_location == gl_int.

:- impure func get_uniform_location(program_id, string) = uniform_location.
:- mode get_uniform_location(in, in) = out is semidet.

:- impure pred uniform_1f(uniform_location::in, float::in) is det.
:- impure pred uniform_2f(uniform_location::in, float::in, float::in) is det.
:- impure pred uniform_3f(uniform_location::in, float::in, float::in, float::in) is det.
:- impure pred uniform_4f(uniform_location::in, float::in, float::in, float::in, float::in) is det.

:- impure pred uniform_1i(uniform_location::in, int::in) is det.
:- impure pred uniform_2i(uniform_location::in, int::in, int::in) is det.
:- impure pred uniform_3i(uniform_location::in, int::in, int::in, int::in) is det.
:- impure pred uniform_4i(uniform_location::in, int::in, int::in, int::in, int::in) is det.

:- impure pred uniform_mat4(
	uniform_location::in,
	float::in, float::in, float::in, float::in,
	float::in, float::in, float::in, float::in,
	float::in, float::in, float::in, float::in,
	float::in, float::in, float::in, float::in
) is det.

%--------------------------------------------------%
% Enable / Disable

:- type capability
	--->	multisample.

:- pred enable(capability::in, io.io::di, io.io::uo) is det.
:- pred disable(capability::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""glad/glad.h""").

%--------------------------------------------------%
% Types

:- pragma foreign_type("C", gl_int, "GLint").
:- pragma foreign_type("C", gl_uint, "GLuint").

%--------------------------------------------------%
% Viewport

:- pragma inline(viewport/6).
:- pragma foreign_proc("C", viewport(X::in, Y::in, W::in, H::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glViewport(X, Y, W, H);
	IO1 = IO0;
	"
).

%--------------------------------------------------%
% Clearing

:- pragma inline(clear_color/6).
:- pragma foreign_proc("C", clear_color(R::in, G::in, B::in, A::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glClearColor((float) R, (float) G, (float) B, (float) A);
	IO1 = IO0;
	"
).

:- pragma inline(clear/2).
:- pragma foreign_proc("C", clear(IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glClear(
		GL_COLOR_BUFFER_BIT |
		GL_DEPTH_BUFFER_BIT |
		GL_STENCIL_BUFFER_BIT
	);
	IO1 = IO0;
	"
).

:- pragma inline(clear_color/6).
:- pragma foreign_proc("C", clear_with_color(R::in, G::in, B::in, A::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glClearColor((float) R, (float) G, (float) B, (float) A);
	glClear(
		GL_COLOR_BUFFER_BIT |
		GL_DEPTH_BUFFER_BIT |
		GL_STENCIL_BUFFER_BIT
	);
	IO1 = IO0;
	"
).

%--------------------------------------------------%
% Shaders / programs
%
% These are left impure, because they aren't meant to be called
% directly, but rather through the shader module.

:- pragma foreign_enum("C", shader_type/0, [
	vertex_shader - "GL_VERTEX_SHADER",
	fragment_shader - "GL_FRAGMENT_SHADER"
]).

:- pragma foreign_proc("C", create_shader(Type::in, ID::out),
	[will_not_call_mercury, thread_safe],
	"
	ID = glCreateShader(Type);
	if (ID != 0) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma foreign_proc("C", shader_source(ID::in, Source::in),
	[will_not_call_mercury, thread_safe],
	"

	glShaderSource(ID, 1, (const char * const *) &Source, NULL);
	"
).

:- pragma foreign_proc("C", compile_shader(ID::in),
	[will_not_call_mercury, thread_safe],
	"glCompileShader(ID)"
).

:- pragma foreign_proc("C", shader_compile_error(ID::in, ErrStr::out),
	[will_not_call_mercury, thread_safe],
	"
	GLint success;
	glGetShaderiv(ID, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLint logLength;
		glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &logLength);
		char *infoLog = calloc(logLength, sizeof(char));
		glGetShaderInfoLog(ID, logLength, NULL, infoLog);
		ErrStr = infoLog;
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma foreign_proc("C", delete_shader(ID::in),
	[will_not_call_mercury, thread_safe],
	"glDeleteShader(ID);"
).

:- pragma foreign_proc("C", create_program(ID::out),
	[will_not_call_mercury, thread_safe],
	"
	ID = glCreateProgram();
	if (ID != 0) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma foreign_proc("C", use_program(ID::in),
	[will_not_call_mercury, thread_safe],
	"glUseProgram(ID);"
).

:- pragma foreign_proc("C", delete_program(ID::in),
	[will_not_call_mercury, thread_safe],
	"glDeleteProgram(ID);"
).

:- pragma foreign_proc("C", attach_shader(ProgramID::in, ShaderID::in),
	[will_not_call_mercury, thread_safe],
	"glAttachShader(ProgramID, ShaderID);"
).

:- pragma foreign_proc("C", link_program(ProgramID::in),
	[will_not_call_mercury, thread_safe],
	"glLinkProgram(ProgramID);"
).

:- pragma foreign_proc("C", program_link_error(ID::in, ErrStr::out),
	[will_not_call_mercury, thread_safe],
	"
	GLint success;
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success) {
		GLint logLength;
		glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &logLength);
		char *infoLog = calloc(logLength, sizeof(char));
		glGetProgramInfoLog(ID, logLength, NULL, infoLog);
		ErrStr = infoLog;
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

% Uniforms

:- pragma foreign_proc("C",
	get_uniform_location(ProgramID::in, Name::in) = (Location::out),
	[will_not_call_mercury, thread_safe],
	"
	Location = glGetUniformLocation(ProgramID, Name);
	if (Location == -1) {
		SUCCESS_INDICATOR = 0;
	} else {
		SUCCESS_INDICATOR = 1;
	}
	"
).

:- pragma foreign_proc("C", uniform_1f(Location::in, V1::in),
	[will_not_call_mercury, thread_safe],
	"glUniform1f(Location, (float) V1);"
).
:- pragma foreign_proc("C", uniform_2f(Location::in, V1::in, V2::in),
	[will_not_call_mercury, thread_safe],
	"glUniform2f(Location, (float) V1, (float) V2);"
).
:- pragma foreign_proc("C", uniform_3f(Location::in, V1::in, V2::in, V3::in),
	[will_not_call_mercury, thread_safe],
	"glUniform3f(Location, (float) V1, (float) V2, (float) V3);"
).
:- pragma foreign_proc("C", uniform_4f(Location::in, V1::in, V2::in, V3::in, V4::in),
	[will_not_call_mercury, thread_safe],
	"glUniform4f(Location, (float) V1, (float) V2, (float) V3, (float) V4);"
).

:- pragma foreign_proc("C", uniform_1i(Location::in, Value::in),
	[will_not_call_mercury, thread_safe],
	"glUniform1i(Location, Value);"
).
:- pragma foreign_proc("C", uniform_2i(Location::in, V1::in, V2::in),
	[will_not_call_mercury, thread_safe],
	"glUniform2i(Location, V1, V2);"
).
:- pragma foreign_proc("C", uniform_3i(Location::in, V1::in, V2::in, V3::in),
	[will_not_call_mercury, thread_safe],
	"glUniform3i(Location, V1, V2, V3);"
).
:- pragma foreign_proc("C", uniform_4i(Location::in, V1::in, V2::in, V3::in, V4::in),
	[will_not_call_mercury, thread_safe],
	"glUniform4i(Location, V1, V2, V3, V4);"
).

:- pragma foreign_proc("C",
	uniform_mat4(
		Location::in,
		V11::in, V12::in, V13::in, V14::in,
		V21::in, V22::in, V23::in, V24::in,
		V31::in, V32::in, V33::in, V34::in,
		V41::in, V42::in, V43::in, V44::in
	),
	[will_not_call_mercury, thread_safe],
	"
	const GLfloat mat[16] = {
		(GLfloat) V11, (GLfloat) V12, (GLfloat) V13, (GLfloat) V14,
		(GLfloat) V21, (GLfloat) V22, (GLfloat) V23, (GLfloat) V24,
		(GLfloat) V31, (GLfloat) V32, (GLfloat) V33, (GLfloat) V34,
		(GLfloat) V41, (GLfloat) V42, (GLfloat) V43, (GLfloat) V44
	};
	glUniformMatrix4fv(Location, 1, 0, (const GLfloat *) mat);
	"
).

%--------------------------------------------------%
% Enable / Disable

:- pragma foreign_enum("C", capability/0, [
	multisample - "GL_MULTISAMPLE"
]).

:- pragma foreign_proc("C", enable(Cap::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glEnable(Cap);
	IO1 = IO0;
	"
).

:- pragma foreign_proc("C", disable(Cap::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury, thread_safe],
	"
	glDisable(Cap);
	IO1 = IO0;
	"
).

%--------------------------------------------------%
:- end_module gl.
