%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: stbi.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module stbi.

%--------------------------------------------------%
:- interface.
:- use_module io, maybe.

:- type foreign_image.
:- type image
	--->	image(
			pointer :: foreign_image,
			width :: int,
			height :: int,
			n :: int
		).

:- pred load(string, maybe.maybe(image), io.io, io.io).
:- mode load(in, out, di, uo) is det.

:- pred image_free(image::in, io.io::di, io.io::uo) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#define STB_IMAGE_STATIC").
:- pragma foreign_decl("C", "#define STBI_ONLY_PNG").
:- pragma foreign_decl("C", "#define STB_IMAGE_IMPLEMENTATION").
:- pragma foreign_decl("C", "#include ""stb_image.h""").

:- pragma foreign_type("C", foreign_image, "unsigned char *").

:- impure pred stbi_load(string::in, foreign_image::out, int::out, int::out, int::out) is semidet.
:- pragma foreign_proc("C", stbi_load(Filename::in, Pointer::out, W::out, H::out, N::out),
	[will_not_call_mercury, thread_safe],
	"
	int w, h, n;
	Pointer = stbi_load(Filename, &w, &h, &n, 1);
	if (Pointer) {
		SUCCESS_INDICATOR = 1;
		W = w;
		H = h;
		N = n;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma promise_pure(load/4).
load(Filename, MaybeImage, !IO) :-
	(	impure stbi_load(Filename, Pointer, W, H, N)
	->	MaybeImage = maybe.yes(image(Pointer, W, H, N))
	;	MaybeImage = maybe.no
	).

:- impure pred stbi_image_free(foreign_image::in) is det.
:- pragma foreign_proc("C", stbi_image_free(Image::in),
	[will_not_call_mercury, thread_safe],
	"stbi_image_free(Image);"
).

:- pragma promise_pure(image_free/3).
image_free(Image, !IO) :-
	impure stbi_image_free(Image ^ pointer).

%--------------------------------------------------%
:- end_module stbi.
