%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.keyboard.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%
:- module sdl.keyboard.

%--------------------------------------------------%
:- interface.

:- type keysym.

    % Possibly multiple keymods ORed together into an uint16.
:- type keymods == uint16.

:- type keymod
	--->	kmod_lshift
	;	kmod_rshift
	;	kmod_lctrl
	;	kmod_rctrl
	;	kmod_lalt
	;	kmod_ralt
	;	kmod_lgui
	;	kmod_rgui
	;	kmod_num
	;	kmod_caps
	;	kmod_mode
	;	kmod_ctrl
	;	kmod_shift
	;	kmod_alt
	;	kmod_gui.

:- type scancode
	--->	scancode_unknown
	;	scancode_a
	;	scancode_b
	;	scancode_c
	;	scancode_d
	;	scancode_e
	;	scancode_f
	;	scancode_g
	;	scancode_h
	;	scancode_i
	;	scancode_j
	;	scancode_k
	;	scancode_l
	;	scancode_m
	;	scancode_n
	;	scancode_o
	;	scancode_p
	;	scancode_q
	;	scancode_r
	;	scancode_s
	;	scancode_t
	;	scancode_u
	;	scancode_v
	;	scancode_w
	;	scancode_x
	;	scancode_y
	;	scancode_z
	;	scancode_1
	;	scancode_2
	;	scancode_3
	;	scancode_4
	;	scancode_5
	;	scancode_6
	;	scancode_7
	;	scancode_8
	;	scancode_9
	;	scancode_0
	;	scancode_return
	;	scancode_escape
	;	scancode_backspace
	;	scancode_tab
	;	scancode_space
	;	scancode_minus
	;	scancode_equals
	;	scancode_leftbracket
	;	scancode_rightbracket
	;	scancode_backslash
	;	scancode_nonushash
	;	scancode_semicolon
	;	scancode_apostrophe
	;	scancode_grave
	;	scancode_comma
	;	scancode_period
	;	scancode_slash
	;	scancode_capslock
	;	scancode_f1
	;	scancode_f2
	;	scancode_f3
	;	scancode_f4
	;	scancode_f5
	;	scancode_f6
	;	scancode_f7
	;	scancode_f8
	;	scancode_f9
	;	scancode_f10
	;	scancode_f11
	;	scancode_f12
	;	scancode_printscreen
	;	scancode_scrolllock
	;	scancode_pause
	;	scancode_insert
	;	scancode_home
	;	scancode_pageup
	;	scancode_delete
	;	scancode_end
	;	scancode_pagedown
	;	scancode_right
	;	scancode_left
	;	scancode_down
	;	scancode_up
	;	scancode_numlockclear
	;	scancode_kp_divide
	;	scancode_kp_multiply
	;	scancode_kp_minus
	;	scancode_kp_plus
	;	scancode_kp_enter
	;	scancode_kp_1
	;	scancode_kp_2
	;	scancode_kp_3
	;	scancode_kp_4
	;	scancode_kp_5
	;	scancode_kp_6
	;	scancode_kp_7
	;	scancode_kp_8
	;	scancode_kp_9
	;	scancode_kp_0
	;	scancode_kp_period
	;	scancode_nonusbackslash
	;	scancode_application
	;	scancode_power
	;	scancode_kp_equals
	;	scancode_f13
	;	scancode_f14
	;	scancode_f15
	;	scancode_f16
	;	scancode_f17
	;	scancode_f18
	;	scancode_f19
	;	scancode_f20
	;	scancode_f21
	;	scancode_f22
	;	scancode_f23
	;	scancode_f24
	;	scancode_execute
	;	scancode_help
	;	scancode_menu
	;	scancode_select
	;	scancode_stop
	;	scancode_again
	;	scancode_undo
	;	scancode_cut
	;	scancode_copy
	;	scancode_paste
	;	scancode_find
	;	scancode_mute
	;	scancode_volumeup
	;	scancode_volumedown
	;	scancode_kp_comma
	;	scancode_kp_equalsas400
	;	scancode_international1
	;	scancode_international2
	;	scancode_international3
	;	scancode_international4
	;	scancode_international5
	;	scancode_international6
	;	scancode_international7
	;	scancode_international8
	;	scancode_international9
	;	scancode_lang1
	;	scancode_lang2
	;	scancode_lang3
	;	scancode_lang4
	;	scancode_lang5
	;	scancode_lang6
	;	scancode_lang7
	;	scancode_lang8
	;	scancode_lang9
	;	scancode_alterase
	;	scancode_sysreq
	;	scancode_cancel
	;	scancode_clear
	;	scancode_prior
	;	scancode_return2
	;	scancode_separator
	;	scancode_out
	;	scancode_oper
	;	scancode_clearagain
	;	scancode_crsel
	;	scancode_exsel
	;	scancode_kp_00
	;	scancode_kp_000
	;	scancode_thousandsseparator
	;	scancode_decimalseparator
	;	scancode_currencyunit
	;	scancode_currencysubunit
	;	scancode_kp_leftparen
	;	scancode_kp_rightparen
	;	scancode_kp_leftbrace
	;	scancode_kp_rightbrace
	;	scancode_kp_tab
	;	scancode_kp_backspace
	;	scancode_kp_a
	;	scancode_kp_b
	;	scancode_kp_c
	;	scancode_kp_d
	;	scancode_kp_e
	;	scancode_kp_f
	;	scancode_kp_xor
	;	scancode_kp_power
	;	scancode_kp_percent
	;	scancode_kp_less
	;	scancode_kp_greater
	;	scancode_kp_ampersand
	;	scancode_kp_dblampersand
	;	scancode_kp_verticalbar
	;	scancode_kp_dblverticalbar
	;	scancode_kp_colon
	;	scancode_kp_hash
	;	scancode_kp_space
	;	scancode_kp_at
	;	scancode_kp_exclam
	;	scancode_kp_memstore
	;	scancode_kp_memrecall
	;	scancode_kp_memclear
	;	scancode_kp_memadd
	;	scancode_kp_memsubtract
	;	scancode_kp_memmultiply
	;	scancode_kp_memdivide
	;	scancode_kp_plusminus
	;	scancode_kp_clear
	;	scancode_kp_clearentry
	;	scancode_kp_binary
	;	scancode_kp_octal
	;	scancode_kp_decimal
	;	scancode_kp_hexadecimal
	;	scancode_lctrl
	;	scancode_lshift
	;	scancode_lalt
	;	scancode_lgui
	;	scancode_rctrl
	;	scancode_rshift
	;	scancode_ralt
	;	scancode_rgui
	;	scancode_mode
	;	scancode_audionext
	;	scancode_audioprev
	;	scancode_audiostop
	;	scancode_audioplay
	;	scancode_audiomute
	;	scancode_mediaselect
	;	scancode_www
	;	scancode_mail
	;	scancode_calculator
	;	scancode_computer
	;	scancode_ac_search
	;	scancode_ac_home
	;	scancode_ac_back
	;	scancode_ac_forward
	;	scancode_ac_stop
	;	scancode_ac_refresh
	;	scancode_ac_bookmarks
	;	scancode_brightnessdown
	;	scancode_brightnessup
	;	scancode_displayswitch
	;	scancode_kbdillumtoggle
	;	scancode_kbdillumdown
	;	scancode_kbdillumup
	;	scancode_eject
	;	scancode_sleep
	;	scancode_app1
	;	scancode_app2
	;	scancode_audiorewind
	;	scancode_audiofastforward.

	% has_keymod(Mods, Modifier):
	%
	% True if `Mods` contains `Modifier`.
	%
:- pred has_keymod(keymods::in, keymod::in) is semidet.

	% sdl_keysym(Keysym, Scancode, Keycode, Mods):
	%
	% Unpack a `sdl_keysym`.
	%
:- pred keysym(keysym::in, scancode::out, keymods::out) is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").

:- pragma foreign_type("C", keysym, "SDL_Keysym").

:- pragma foreign_enum(
	"C",
	keymod/0,
	[
		kmod_lshift - "KMOD_LSHIFT",
		kmod_rshift - "KMOD_RSHIFT",
		kmod_lctrl - "KMOD_LCTRL",
		kmod_rctrl - "KMOD_RCTRL",
		kmod_lalt - "KMOD_LALT",
		kmod_ralt - "KMOD_RALT",
		kmod_lgui - "KMOD_LGUI",
		kmod_rgui - "KMOD_RGUI",
		kmod_num - "KMOD_NUM",
		kmod_caps - "KMOD_CAPS",
		kmod_mode - "KMOD_MODE",
		kmod_ctrl - "KMOD_CTRL",
		kmod_shift - "KMOD_SHIFT",
		kmod_alt - "KMOD_ALT",
		kmod_gui - "KMOD_GUI"
	]
).

:- pragma foreign_enum(
	"C",
	scancode/0,
	[
		scancode_unknown - "SDL_SCANCODE_UNKNOWN",
		scancode_a - "SDL_SCANCODE_A",
		scancode_b - "SDL_SCANCODE_B",
		scancode_c - "SDL_SCANCODE_C",
		scancode_d - "SDL_SCANCODE_D",
		scancode_e - "SDL_SCANCODE_E",
		scancode_f - "SDL_SCANCODE_F",
		scancode_g - "SDL_SCANCODE_G",
		scancode_h - "SDL_SCANCODE_H",
		scancode_i - "SDL_SCANCODE_I",
		scancode_j - "SDL_SCANCODE_J",
		scancode_k - "SDL_SCANCODE_K",
		scancode_l - "SDL_SCANCODE_L",
		scancode_m - "SDL_SCANCODE_M",
		scancode_n - "SDL_SCANCODE_N",
		scancode_o - "SDL_SCANCODE_O",
		scancode_p - "SDL_SCANCODE_P",
		scancode_q - "SDL_SCANCODE_Q",
		scancode_r - "SDL_SCANCODE_R",
		scancode_s - "SDL_SCANCODE_S",
		scancode_t - "SDL_SCANCODE_T",
		scancode_u - "SDL_SCANCODE_U",
		scancode_v - "SDL_SCANCODE_V",
		scancode_w - "SDL_SCANCODE_W",
		scancode_x - "SDL_SCANCODE_X",
		scancode_y - "SDL_SCANCODE_Y",
		scancode_z - "SDL_SCANCODE_Z",
		scancode_1 - "SDL_SCANCODE_1",
		scancode_2 - "SDL_SCANCODE_2",
		scancode_3 - "SDL_SCANCODE_3",
		scancode_4 - "SDL_SCANCODE_4",
		scancode_5 - "SDL_SCANCODE_5",
		scancode_6 - "SDL_SCANCODE_6",
		scancode_7 - "SDL_SCANCODE_7",
		scancode_8 - "SDL_SCANCODE_8",
		scancode_9 - "SDL_SCANCODE_9",
		scancode_0 - "SDL_SCANCODE_0",
		scancode_return - "SDL_SCANCODE_RETURN",
		scancode_escape - "SDL_SCANCODE_ESCAPE",
		scancode_backspace - "SDL_SCANCODE_BACKSPACE",
		scancode_tab - "SDL_SCANCODE_TAB",
		scancode_space - "SDL_SCANCODE_SPACE",
		scancode_minus - "SDL_SCANCODE_MINUS",
		scancode_equals - "SDL_SCANCODE_EQUALS",
		scancode_leftbracket - "SDL_SCANCODE_LEFTBRACKET",
		scancode_rightbracket - "SDL_SCANCODE_RIGHTBRACKET",
		scancode_backslash - "SDL_SCANCODE_BACKSLASH",
		scancode_nonushash - "SDL_SCANCODE_NONUSHASH",
		scancode_semicolon - "SDL_SCANCODE_SEMICOLON",
		scancode_apostrophe - "SDL_SCANCODE_APOSTROPHE",
		scancode_grave - "SDL_SCANCODE_GRAVE",
		scancode_comma - "SDL_SCANCODE_COMMA",
		scancode_period - "SDL_SCANCODE_PERIOD",
		scancode_slash - "SDL_SCANCODE_SLASH",
		scancode_capslock - "SDL_SCANCODE_CAPSLOCK",
		scancode_f1 - "SDL_SCANCODE_F1",
		scancode_f2 - "SDL_SCANCODE_F2",
		scancode_f3 - "SDL_SCANCODE_F3",
		scancode_f4 - "SDL_SCANCODE_F4",
		scancode_f5 - "SDL_SCANCODE_F5",
		scancode_f6 - "SDL_SCANCODE_F6",
		scancode_f7 - "SDL_SCANCODE_F7",
		scancode_f8 - "SDL_SCANCODE_F8",
		scancode_f9 - "SDL_SCANCODE_F9",
		scancode_f10 - "SDL_SCANCODE_F10",
		scancode_f11 - "SDL_SCANCODE_F11",
		scancode_f12 - "SDL_SCANCODE_F12",
		scancode_printscreen - "SDL_SCANCODE_PRINTSCREEN",
		scancode_scrolllock - "SDL_SCANCODE_SCROLLLOCK",
		scancode_pause - "SDL_SCANCODE_PAUSE",
		scancode_insert - "SDL_SCANCODE_INSERT",
		scancode_home - "SDL_SCANCODE_HOME",
		scancode_pageup - "SDL_SCANCODE_PAGEUP",
		scancode_delete - "SDL_SCANCODE_DELETE",
		scancode_end - "SDL_SCANCODE_END",
		scancode_pagedown - "SDL_SCANCODE_PAGEDOWN",
		scancode_right - "SDL_SCANCODE_RIGHT",
		scancode_left - "SDL_SCANCODE_LEFT",
		scancode_down - "SDL_SCANCODE_DOWN",
		scancode_up - "SDL_SCANCODE_UP",
		scancode_numlockclear - "SDL_SCANCODE_NUMLOCKCLEAR",
		scancode_kp_divide - "SDL_SCANCODE_KP_DIVIDE",
		scancode_kp_multiply - "SDL_SCANCODE_KP_MULTIPLY",
		scancode_kp_minus - "SDL_SCANCODE_KP_MINUS",
		scancode_kp_plus - "SDL_SCANCODE_KP_PLUS",
		scancode_kp_enter - "SDL_SCANCODE_KP_ENTER",
		scancode_kp_1 - "SDL_SCANCODE_KP_1",
		scancode_kp_2 - "SDL_SCANCODE_KP_2",
		scancode_kp_3 - "SDL_SCANCODE_KP_3",
		scancode_kp_4 - "SDL_SCANCODE_KP_4",
		scancode_kp_5 - "SDL_SCANCODE_KP_5",
		scancode_kp_6 - "SDL_SCANCODE_KP_6",
		scancode_kp_7 - "SDL_SCANCODE_KP_7",
		scancode_kp_8 - "SDL_SCANCODE_KP_8",
		scancode_kp_9 - "SDL_SCANCODE_KP_9",
		scancode_kp_0 - "SDL_SCANCODE_KP_0",
		scancode_kp_period - "SDL_SCANCODE_KP_PERIOD",
		scancode_nonusbackslash - "SDL_SCANCODE_NONUSBACKSLASH",
		scancode_application - "SDL_SCANCODE_APPLICATION",
		scancode_power - "SDL_SCANCODE_POWER",
		scancode_kp_equals - "SDL_SCANCODE_KP_EQUALS",
		scancode_f13 - "SDL_SCANCODE_F13",
		scancode_f14 - "SDL_SCANCODE_F14",
		scancode_f15 - "SDL_SCANCODE_F15",
		scancode_f16 - "SDL_SCANCODE_F16",
		scancode_f17 - "SDL_SCANCODE_F17",
		scancode_f18 - "SDL_SCANCODE_F18",
		scancode_f19 - "SDL_SCANCODE_F19",
		scancode_f20 - "SDL_SCANCODE_F20",
		scancode_f21 - "SDL_SCANCODE_F21",
		scancode_f22 - "SDL_SCANCODE_F22",
		scancode_f23 - "SDL_SCANCODE_F23",
		scancode_f24 - "SDL_SCANCODE_F24",
		scancode_execute - "SDL_SCANCODE_EXECUTE",
		scancode_help - "SDL_SCANCODE_HELP",
		scancode_menu - "SDL_SCANCODE_MENU",
		scancode_select - "SDL_SCANCODE_SELECT",
		scancode_stop - "SDL_SCANCODE_STOP",
		scancode_again - "SDL_SCANCODE_AGAIN",
		scancode_undo - "SDL_SCANCODE_UNDO",
		scancode_cut - "SDL_SCANCODE_CUT",
		scancode_copy - "SDL_SCANCODE_COPY",
		scancode_paste - "SDL_SCANCODE_PASTE",
		scancode_find - "SDL_SCANCODE_FIND",
		scancode_mute - "SDL_SCANCODE_MUTE",
		scancode_volumeup - "SDL_SCANCODE_VOLUMEUP",
		scancode_volumedown - "SDL_SCANCODE_VOLUMEDOWN",
		scancode_kp_comma - "SDL_SCANCODE_KP_COMMA",
		scancode_kp_equalsas400 - "SDL_SCANCODE_KP_EQUALSAS400",
		scancode_international1 - "SDL_SCANCODE_INTERNATIONAL1",
		scancode_international2 - "SDL_SCANCODE_INTERNATIONAL2",
		scancode_international3 - "SDL_SCANCODE_INTERNATIONAL3",
		scancode_international4 - "SDL_SCANCODE_INTERNATIONAL4",
		scancode_international5 - "SDL_SCANCODE_INTERNATIONAL5",
		scancode_international6 - "SDL_SCANCODE_INTERNATIONAL6",
		scancode_international7 - "SDL_SCANCODE_INTERNATIONAL7",
		scancode_international8 - "SDL_SCANCODE_INTERNATIONAL8",
		scancode_international9 - "SDL_SCANCODE_INTERNATIONAL9",
		scancode_lang1 - "SDL_SCANCODE_LANG1",
		scancode_lang2 - "SDL_SCANCODE_LANG2",
		scancode_lang3 - "SDL_SCANCODE_LANG3",
		scancode_lang4 - "SDL_SCANCODE_LANG4",
		scancode_lang5 - "SDL_SCANCODE_LANG5",
		scancode_lang6 - "SDL_SCANCODE_LANG6",
		scancode_lang7 - "SDL_SCANCODE_LANG7",
		scancode_lang8 - "SDL_SCANCODE_LANG8",
		scancode_lang9 - "SDL_SCANCODE_LANG9",
		scancode_alterase - "SDL_SCANCODE_ALTERASE",
		scancode_sysreq - "SDL_SCANCODE_SYSREQ",
		scancode_cancel - "SDL_SCANCODE_CANCEL",
		scancode_clear - "SDL_SCANCODE_CLEAR",
		scancode_prior - "SDL_SCANCODE_PRIOR",
		scancode_return2 - "SDL_SCANCODE_RETURN2",
		scancode_separator - "SDL_SCANCODE_SEPARATOR",
		scancode_out - "SDL_SCANCODE_OUT",
		scancode_oper - "SDL_SCANCODE_OPER",
		scancode_clearagain - "SDL_SCANCODE_CLEARAGAIN",
		scancode_crsel - "SDL_SCANCODE_CRSEL",
		scancode_exsel - "SDL_SCANCODE_EXSEL",
		scancode_kp_00 - "SDL_SCANCODE_KP_00",
		scancode_kp_000 - "SDL_SCANCODE_KP_000",
		scancode_thousandsseparator - "SDL_SCANCODE_THOUSANDSSEPARATOR",
		scancode_decimalseparator - "SDL_SCANCODE_DECIMALSEPARATOR",
		scancode_currencyunit - "SDL_SCANCODE_CURRENCYUNIT",
		scancode_currencysubunit - "SDL_SCANCODE_CURRENCYSUBUNIT",
		scancode_kp_leftparen - "SDL_SCANCODE_KP_LEFTPAREN",
		scancode_kp_rightparen - "SDL_SCANCODE_KP_RIGHTPAREN",
		scancode_kp_leftbrace - "SDL_SCANCODE_KP_LEFTBRACE",
		scancode_kp_rightbrace - "SDL_SCANCODE_KP_RIGHTBRACE",
		scancode_kp_tab - "SDL_SCANCODE_KP_TAB",
		scancode_kp_backspace - "SDL_SCANCODE_KP_BACKSPACE",
		scancode_kp_a - "SDL_SCANCODE_KP_A",
		scancode_kp_b - "SDL_SCANCODE_KP_B",
		scancode_kp_c - "SDL_SCANCODE_KP_C",
		scancode_kp_d - "SDL_SCANCODE_KP_D",
		scancode_kp_e - "SDL_SCANCODE_KP_E",
		scancode_kp_f - "SDL_SCANCODE_KP_F",
		scancode_kp_xor - "SDL_SCANCODE_KP_XOR",
		scancode_kp_power - "SDL_SCANCODE_KP_POWER",
		scancode_kp_percent - "SDL_SCANCODE_KP_PERCENT",
		scancode_kp_less - "SDL_SCANCODE_KP_LESS",
		scancode_kp_greater - "SDL_SCANCODE_KP_GREATER",
		scancode_kp_ampersand - "SDL_SCANCODE_KP_AMPERSAND",
		scancode_kp_dblampersand - "SDL_SCANCODE_KP_DBLAMPERSAND",
		scancode_kp_verticalbar - "SDL_SCANCODE_KP_VERTICALBAR",
		scancode_kp_dblverticalbar - "SDL_SCANCODE_KP_DBLVERTICALBAR",
		scancode_kp_colon - "SDL_SCANCODE_KP_COLON",
		scancode_kp_hash - "SDL_SCANCODE_KP_HASH",
		scancode_kp_space - "SDL_SCANCODE_KP_SPACE",
		scancode_kp_at - "SDL_SCANCODE_KP_AT",
		scancode_kp_exclam - "SDL_SCANCODE_KP_EXCLAM",
		scancode_kp_memstore - "SDL_SCANCODE_KP_MEMSTORE",
		scancode_kp_memrecall - "SDL_SCANCODE_KP_MEMRECALL",
		scancode_kp_memclear - "SDL_SCANCODE_KP_MEMCLEAR",
		scancode_kp_memadd - "SDL_SCANCODE_KP_MEMADD",
		scancode_kp_memsubtract - "SDL_SCANCODE_KP_MEMSUBTRACT",
		scancode_kp_memmultiply - "SDL_SCANCODE_KP_MEMMULTIPLY",
		scancode_kp_memdivide - "SDL_SCANCODE_KP_MEMDIVIDE",
		scancode_kp_plusminus - "SDL_SCANCODE_KP_PLUSMINUS",
		scancode_kp_clear - "SDL_SCANCODE_KP_CLEAR",
		scancode_kp_clearentry - "SDL_SCANCODE_KP_CLEARENTRY",
		scancode_kp_binary - "SDL_SCANCODE_KP_BINARY",
		scancode_kp_octal - "SDL_SCANCODE_KP_OCTAL",
		scancode_kp_decimal - "SDL_SCANCODE_KP_DECIMAL",
		scancode_kp_hexadecimal - "SDL_SCANCODE_KP_HEXADECIMAL",
		scancode_lctrl - "SDL_SCANCODE_LCTRL",
		scancode_lshift - "SDL_SCANCODE_LSHIFT",
		scancode_lalt - "SDL_SCANCODE_LALT",
		scancode_lgui - "SDL_SCANCODE_LGUI",
		scancode_rctrl - "SDL_SCANCODE_RCTRL",
		scancode_rshift - "SDL_SCANCODE_RSHIFT",
		scancode_ralt - "SDL_SCANCODE_RALT",
		scancode_rgui - "SDL_SCANCODE_RGUI",
		scancode_mode - "SDL_SCANCODE_MODE",
		scancode_audionext - "SDL_SCANCODE_AUDIONEXT",
		scancode_audioprev - "SDL_SCANCODE_AUDIOPREV",
		scancode_audiostop - "SDL_SCANCODE_AUDIOSTOP",
		scancode_audioplay - "SDL_SCANCODE_AUDIOPLAY",
		scancode_audiomute - "SDL_SCANCODE_AUDIOMUTE",
		scancode_mediaselect - "SDL_SCANCODE_MEDIASELECT",
		scancode_www - "SDL_SCANCODE_WWW",
		scancode_mail - "SDL_SCANCODE_MAIL",
		scancode_calculator - "SDL_SCANCODE_CALCULATOR",
		scancode_computer - "SDL_SCANCODE_COMPUTER",
		scancode_ac_search - "SDL_SCANCODE_AC_SEARCH",
		scancode_ac_home - "SDL_SCANCODE_AC_HOME",
		scancode_ac_back - "SDL_SCANCODE_AC_BACK",
		scancode_ac_forward - "SDL_SCANCODE_AC_FORWARD",
		scancode_ac_stop - "SDL_SCANCODE_AC_STOP",
		scancode_ac_refresh - "SDL_SCANCODE_AC_REFRESH",
		scancode_ac_bookmarks - "SDL_SCANCODE_AC_BOOKMARKS",
		scancode_brightnessdown - "SDL_SCANCODE_BRIGHTNESSDOWN",
		scancode_brightnessup - "SDL_SCANCODE_BRIGHTNESSUP",
		scancode_displayswitch - "SDL_SCANCODE_DISPLAYSWITCH",
		scancode_kbdillumtoggle - "SDL_SCANCODE_KBDILLUMTOGGLE",
		scancode_kbdillumdown - "SDL_SCANCODE_KBDILLUMDOWN",
		scancode_kbdillumup - "SDL_SCANCODE_KBDILLUMUP",
		scancode_eject - "SDL_SCANCODE_EJECT",
		scancode_sleep - "SDL_SCANCODE_SLEEP",
		scancode_app1 - "SDL_SCANCODE_APP1",
		scancode_app2 - "SDL_SCANCODE_APP2",
		scancode_audiorewind - "SDL_SCANCODE_AUDIOREWIND",
		scancode_audiofastforward - "SDL_SCANCODE_AUDIOFASTFORWARD"
	]
).

:- pragma foreign_proc(
	"C",
	has_keymod(Mods::in, Modifier::in),
	[promise_pure],
	"
	if (Mods & Modifier) {
		SUCCESS_INDICATOR = 1;
	} else {
		SUCCESS_INDICATOR = 0;
	}
	"
).

:- pragma foreign_proc(
	"C",
	keysym(Keysym::in, Scancode::out, Mods::out),
	[promise_pure],
	"
	Scancode = Keysym.scancode;
	Mods = Keysym.mod;
	"
).

:- end_module sdl.keyboard.
