%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: sdl.window.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module sdl.window.

%--------------------------------------------------%
:- interface.
:- import_module list.
:- use_module io, maybe.

:- type window_flag
	--->	fullscreen
	;	opengl
	;	shown
	;	hidden
	;	borderless
	;	resizable
	;	minimized
	;	maximized
	;	input_grabbed
	;	input_focus
	;	mouse_focus
	;	fullscreen_desktop
	;	foreign
	;	allow_highdpi
	;	mouse_capture
	;	always_on_top
	;	skip_taskbar
	;	utility
	;	tooltip
	;	popup_menu
	;	vulkan.

:- type window_flags == list(window_flag).

:- type window_id.

:- type window.

:- pred create_window(string, int, int, int, int, window_flags, maybe.maybe_error(window), io.io, io.io).
:- mode create_window(in, in, in, in, in, in, out, di, uo) is det.

:- pred destroy_window(window::in, io.io::di, io.io::uo) is det.

	% call_with_window(Title, X, Y, Width, Height, Flags, P, Result, !IO):
	%
	% Call predicate P with a window.
	%
:- pred call_with_window(
	string::in,
	int::in, int::in,
	int::in, int::in,
	window_flags::in,
	pred(window, T, io.io, io.io)::pred(in, out, di, uo) is det,
	maybe.maybe_error(T)::out,
	io.io::di, io.io::uo
) is det.

	% windowpos_undefined = WindowPosition
	%
	% Use as a position argument to create_window to indicate that
	% you don't care about the position on that axis.
	%
:- func undefined = int is det.

	% windowpos_centered = WindowPosition
	%
	% Use as a position argument to create_window to center the
	% window on that axis.
	%
:- func centered = int is det.

%--------------------------------------------------%
:- implementation.

:- pragma foreign_decl("C", "#include ""SDL2/SDL.h""").
:- pragma foreign_decl("C", "#include ""glad/glad.h""").

%--------------------------------------------------%
% SDL Window

:- type window_id == uint32.

:- pragma foreign_type("C", window, "SDL_Window *").

:- pragma foreign_enum(
	"C",
	window_flag/0,
	[
		fullscreen - "SDL_WINDOW_FULLSCREEN",
		opengl - "SDL_WINDOW_OPENGL",
		shown - "SDL_WINDOW_SHOWN",
		hidden - "SDL_WINDOW_HIDDEN",
		borderless - "SDL_WINDOW_BORDERLESS",
		resizable - "SDL_WINDOW_RESIZABLE",
		minimized - "SDL_WINDOW_MINIMIZED",
		maximized - "SDL_WINDOW_MAXIMIZED",
		input_grabbed - "SDL_WINDOW_INPUT_GRABBED",
		input_focus - "SDL_WINDOW_INPUT_FOCUS",
		mouse_focus - "SDL_WINDOW_MOUSE_FOCUS",
		fullscreen_desktop - "SDL_WINDOW_FULLSCREEN_DESKTOP",
		foreign - "SDL_WINDOW_FOREIGN",
		allow_highdpi - "SDL_WINDOW_ALLOW_HIGHDPI",
		mouse_capture - "SDL_WINDOW_MOUSE_CAPTURE",
		always_on_top - "SDL_WINDOW_ALWAYS_ON_TOP",
		skip_taskbar - "SDL_WINDOW_SKIP_TASKBAR",
		utility - "SDL_WINDOW_UTILITY",
		tooltip - "SDL_WINDOW_TOOLTIP",
		popup_menu - "SDL_WINDOW_POPUP_MENU",
		vulkan - "SDL_WINDOW_VULKAN"
	]
).

:- impure func sdl_create_window(string, int, int, int, int, window_flags) = window is semidet.
:- pragma foreign_proc(
	"C",
	sdl_create_window(Title::in, X::in, Y::in, Width::in, Height::in, Flags::in) = (Window::out),
	[],
	"
	SDL_Window *window = SDL_CreateWindow(
		Title, X, Y, Width, Height, GLTEST_SDL_CombineFlags(Flags)
	);
	if (!window) {
		SUCCESS_INDICATOR = 0;
	} else {
		Window = window;
		SUCCESS_INDICATOR = 1;
	}
	"
).

:- pragma promise_pure(create_window/9).
create_window(Title, X, Y, Width, Height, Flags, WindowResult, !IO) :-
	(	impure Window = sdl_create_window(Title, X, Y, Width, Height, Flags)
	->	WindowResult = maybe.ok(Window)
	;	WindowResult = maybe.error(sdl.get_error(!.IO, !:IO))
	).

:- pragma foreign_proc("C", destroy_window(Window::in, IO0::di, IO1::uo),
	[promise_pure, will_not_call_mercury],
	"
	SDL_DestroyWindow(Window);
	IO1 = IO0;
	"
).

call_with_window(Title, X, Y, Width, Height, Flags, P, Result, !IO) :-
	create_window(Title, X, Y, Width, Height, Flags, WindowResult, !IO),
	(	WindowResult = maybe.ok(Window),
		P(Window, PResult, !IO),
		Result = maybe.ok(PResult),
		destroy_window(Window, !IO)
	;	WindowResult = maybe.error(Err),
		Result = maybe.error(Err)
	).

:- pragma foreign_proc("C", undefined = (Out::out),
	[promise_pure, will_not_call_mercury],
	"Out = SDL_WINDOWPOS_UNDEFINED;"
).

:- pragma foreign_proc("C", centered = (Out::out),
	[promise_pure, will_not_call_mercury],
	"Out = SDL_WINDOWPOS_CENTERED;"
).


:- end_module sdl.window.
