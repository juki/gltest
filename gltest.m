%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: gltest.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
%--------------------------------------------------%

:- module gltest.

%--------------------------------------------------%
:- interface.
:- use_module io.

:- pred main(io.io::di, io.io::uo) is cc_multi.

%--------------------------------------------------%
:- implementation.
:- import_module string, list, bool, uint32, store, exception.
:- use_module maybe.
:- use_module benchmarking.
:- use_module
	sdl,
	sdl.keyboard,
	sdl.window,
	sdl.gl,
	sdl.events.
:- use_module glad.
:- use_module gl.
:- use_module game.
:- use_module shader.

main(!IO) :-
	io.tell("log", R, !IO),
	(	R = io.ok
	->	(try [io(!IO)] (
			sdl.call_with_sdl(
				[sdl.events, sdl.video],
				game_with_sdl,
				Result,
				!IO
			),
			(	Result = maybe.ok(Value),
				(	Value = maybe.ok,
					io.print_line("Game exited succesfully.", !IO)
				;	Value = maybe.error(Err),
					io.format("Game exited with error: %s\n", [s(Err)], !IO)
				)
			;	Result = maybe.error(Err),
				io.format("Failed to init SDL: %s\n", [s(Err)], !IO)
			)
		)
		then true
		catch_any Any ->
			io.tell("errfile", Result, !IO),
			(	Result = io.ok
			->	io.print(Any, !IO),
				io.told(!IO)
			;	true
			)
		)
	;	true
	).

:- pred game_with_sdl(maybe.maybe_error::out, io.io::di, io.io::uo) is det.
game_with_sdl(Result, !IO) :-
	setup_sdl_gl_attributes(SetAttributes, !IO),
	(	SetAttributes = yes,
		sdl.window.call_with_window(
			"My window",
			sdl.window.undefined, sdl.window.undefined,
			800, 600,
			[sdl.window.opengl, sdl.window.resizable, sdl.window.shown],
			game_with_window,
			Res,
			!IO
		),
		(	Res = maybe.ok(Value),
			(	Value = maybe.ok,
				Result = maybe.ok
			;	Value = maybe.error(Err),
				Result = maybe.error(Err)
			)
		;	Res = maybe.error(Err),
			Result = maybe.error(Err)
		)
	;	SetAttributes = no,
		Result = maybe.error("Failed to set attributes for GL context.")
	).

:- pred game_with_window(sdl.window.window, maybe.maybe_error, io.io, io.io).
:- mode game_with_window(in, out, di, uo) is det.
game_with_window(Window, Result, !IO) :-
	sdl.gl.create_context(Window, Context, !IO),
	sdl.gl.make_current(Window, Context, !IO),
	sdl.gl.set_swap_interval(sdl.gl.adaptive_vsync, SwapIntervalResult, !IO),
	glad.load_gl(GladResult, !IO),
	(	GladResult = maybe.ok,
		(	SwapIntervalResult = maybe.ok
		;	SwapIntervalResult = maybe.error(_),
			sdl.gl.set_swap_interval(sdl.gl.vsync, _, !IO)
		),
		event_loop(Window, Context, !IO),
		Result = maybe.ok
	;	GladResult = maybe.error(Err),
		Result = maybe.error(Err)
	),
	sdl.gl.delete_context(Context, !IO).

:- pred setup_sdl_gl_attributes(bool::out, io.io::di, io.io::uo) is det.
setup_sdl_gl_attributes(Success, !IO) :-
	sdl.gl.set_attribute(sdl.gl.context_major_version, 3, MajorV, !IO),
	sdl.gl.set_attribute(sdl.gl.context_minor_version, 3, MinorV, !IO),
	sdl.gl.set_context_profile_mask([sdl.gl.core], Profile, !IO),
	sdl.gl.set_context_flags([sdl.gl.forward_compatible], FC, !IO),
	sdl.gl.set_attribute(sdl.gl.multisamplebuffers, 1, SampleBufs, !IO),
	sdl.gl.set_attribute(sdl.gl.multisamplesamples, 8, Samples, !IO),
	Success = and_list([MajorV, MinorV, Profile, FC, SampleBufs, Samples]).

:- pred event_loop(sdl.window.window, sdl.gl.context, io.io, io.io).
:- mode event_loop(in, in, di, uo) is det.
event_loop(Window, GLContext, !IO) :-
	gl.enable(gl.multisample, !IO),
	game.init(Game, !.IO, !:IO),
	setup_viewport(Window, Game, !IO),
	sdl.events.fold_events(
		[
			sdl.events.idle(idle(Window, GLContext, Game)),
			sdl.events.quit(quit),
			sdl.events.key_up(key_up(Game)),
			sdl.events.window_size_changed(size_changed(Window, Game))
		],
		!IO
	),
	game.destroy(Game, !IO).

:- pred idle(sdl.window.window, sdl.gl.context, game.game) : sdl.events.idle_handler.
:- mode idle(in, in, in) `with_inst` sdl.events.idle_handler.
idle(Window, GLContext, Game, yes, !IO) :-
	game.idle(Window, GLContext, Game, !IO).

:- pred quit : sdl.events.quit_handler.
:- mode quit `with_inst` sdl.events.quit_handler.
quit(no, Timestamp, !IO) :-
	io.format(
		"Quit event at '%d'\n",
		[i(cast_to_int(Timestamp))],
		!IO
	).

:- pred key_up(game.game) : sdl.events.key_up_handler.
:- mode key_up(in) `with_inst` sdl.events.key_up_handler.
key_up(Game, yes, _, _, BState, Repeat, Scancode, Mods, !IO) :-
	game.key_up(BState, Repeat, Scancode, Mods, Game, !IO).

:- pred size_changed(sdl.window.window, game.game) : sdl.events.window_size_changed_handler.
:- mode size_changed(in, in) `with_inst` sdl.events.window_size_changed_handler.
size_changed(Window, Game, yes, _, _, _, _, !IO) :-
	setup_viewport(Window, Game, !IO).

:- pred setup_viewport(sdl.window.window::in, game.game::in, io.io::di, io.io::uo) is det.
setup_viewport(Window, Game, !IO) :-
	sdl.gl.get_drawable_size(Window, W, H, !IO),
	gl.viewport(0, 0, W, H, !IO),
	game.viewport_size_changed(W, H, Game, !IO).

%--------------------------------------------------%
:- end_module gltest.
